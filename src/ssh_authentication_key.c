/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "dm_ssh_server.h"

#define AUTHORIZED_KEY "/etc/dropbear/authorized_keys"

#define string_empty(X) ((X == NULL) || (*X == '\0'))

#define MAX_KEY_FILES 10

char* authKeyFile[MAX_KEY_FILES];
int numAuthKeyFiles = 0;

static bool authKeyFile_exists(char* path) {
    for(int i = 0; i < numAuthKeyFiles; i++) {
        if(strcmp(authKeyFile[i], path) == 0) {
            return true;
        }
    }
    return false;
}

static bool addAuthKeyFile(char* path) {
    bool ret = false;
    if(authKeyFile_exists(path)) {
        goto exit;
    }

    if(numAuthKeyFiles < MAX_KEY_FILES) {
        authKeyFile[numAuthKeyFiles++] = strdup(path);
        ret = true;
    }
exit:
    return ret;
}

static bool query_ssh_user(char* userPath, char** user, int32_t* userId, int32_t* groupId) {
    SAH_TRACEZ_INFO(ME, "ssh_server_query_ssh_group");
    amxb_bus_ctx_t* bus_ctx = amxb_be_who_has("Users");
    amxc_string_t templ_str;
    amxc_var_t res;
    amxc_var_t csv;
    const char* tmp = NULL;
    bool ret = false;

    amxc_string_init(&templ_str, 0);
    amxc_var_init(&res);
    amxc_var_init(&csv);

    when_null_trace(bus_ctx, exit, ERROR, "No bus_ctx");
    if(string_empty(userPath)) {
        goto exit;
    }

    amxc_string_setf(&templ_str, "%s.", userPath);
    amxc_string_replace(&templ_str, "Device.", "", UINT32_MAX);
    amxc_string_replace(&templ_str, "..", ".", UINT32_MAX);
    if(amxb_get(bus_ctx, amxc_string_get(&templ_str, 0), 0, &res, 5) != AMXB_STATUS_OK) {
        SAH_TRACEZ_ERROR(ME, "Failed to get SSH user '%s'", userPath);
        goto exit;
    }

    tmp = GETP_CHAR(&res, "0.0.Username");
    if(tmp == NULL) {
        goto exit;
    }
    *user = strdup(tmp);
    *userId = GETP_INT32(&res, "0.0.UserID");

    amxc_var_set(csv_string_t, &csv, GETP_CHAR(&res, "0.0.GroupParticipation"));
    amxc_var_cast(&csv, AMXC_VAR_ID_LIST);
    amxc_string_setf(&templ_str, "%s.", GET_CHAR(amxc_var_get_first(&csv), NULL));
    if(amxb_get(bus_ctx, amxc_string_get(&templ_str, 0), 0, &res, 5) != AMXB_STATUS_OK) {
        goto exit;
    }

    *groupId = GETP_INT32(&res, "0.0.GroupID");
    ret = true;
exit:
    amxc_string_clean(&templ_str);
    amxc_var_clean(&res);
    amxc_var_clean(&csv);
    return ret;
}

static bool get_key_path(char* user, int32_t uid, int32_t gid, char** keyPath) {
    amxc_string_t path;
    bool ret = false;
    amxc_string_init(&path, 0);

    if(uid == 0) {
        amxc_string_setf(&path, "%s", AUTHORIZED_KEY);
        *keyPath = amxc_string_take_buffer(&path);
        ret = true;
        goto exit;
    }

    if(string_empty(user)) {
        goto exit;
    }

    amxc_string_setf(&path, "/home/%s/.ssh/", user);

    // Check if .ssh directory exists
    if(!amxp_dir_is_directory(amxc_string_get(&path, 0))) {
        // Directory does not exist, create it
        if(amxp_dir_owned_make(amxc_string_get(&path, 0), 0755, uid, gid)) {
            SAH_TRACEZ_ERROR(ME, "Error creating .ssh directory");
            goto exit;
        }
    }

    amxc_string_setf(&path, "/home/%s/.ssh/authorized_keys", user);

    *keyPath = amxc_string_take_buffer(&path);
    ret = true;

exit:
    amxc_string_clean(&path);
    return ret;
}

static int clean_authkeys_files(void) {
    SAH_TRACEZ_INFO(ME, "clean_authkeys_files");
    for(int i = 0; i < numAuthKeyFiles; i++) {
        if(access(authKeyFile[i], F_OK) != -1) {
            unlink(authKeyFile[i]);
        }
        free(authKeyFile[i]);
        authKeyFile[i] = NULL;
    }
    numAuthKeyFiles = 0;
    return numAuthKeyFiles;
}

static int add_server_authkeys_to_file(amxd_object_t* obj) {
    int retval = 0;
    SAH_TRACEZ_INFO(ME, "add_server_authkeys_to_file");
    when_null(obj, exit);
    amxd_object_t* auth_obj = amxd_object_get_child(obj, "AuthorizedKey");
    when_null(auth_obj, exit);
    amxd_object_for_each(instance, it, auth_obj) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        char* userPath = amxd_object_get_value(cstring_t, instance, "User", NULL);
        char* key = amxd_object_get_value(cstring_t, instance, "Key", NULL);
        char* user = NULL;
        char* keyPath = NULL;
        int32_t uid = 0;
        int32_t gid = 0;
        FILE* keyfile = NULL;
        int fd;

        if(string_empty(userPath) || string_empty(key)) {
            goto err;
        }

        SAH_TRACEZ_INFO(ME, "authentication instance: %s  key: %s",
                        amxd_object_get_name(instance, AMXD_OBJECT_NAMED), key);

        if(strcmp(userPath, "root")) {
            if(!query_ssh_user(userPath, &user, &uid, &gid)) {
                SAH_TRACEZ_ERROR(ME, "Failed to query %s", userPath);
                goto err;
            }
        }

        get_key_path(user, uid, gid, &keyPath);
        if(string_empty(keyPath)) {
            goto err;
        }

        if((!authKeyFile_exists(keyPath)) && (numAuthKeyFiles >= MAX_KEY_FILES)) {
            SAH_TRACEZ_ERROR(ME, "Key ignored - Max AuthorizedKey files reached");
            goto err;
        }

        keyfile = fopen(keyPath, "a");
        when_null_trace(keyfile, err, ERROR, "unable to open %s for writing: %s", keyPath, strerror(errno));

        if(fwrite(key, strlen(key), 1, keyfile) != 1) {
            SAH_TRACEZ_ERROR(ME, "authentication instance: %s unable to write key to %s: %s", amxd_object_get_name(instance, AMXD_OBJECT_NAMED), keyPath, strerror(errno));
            fclose(keyfile);
            goto err;
        }

        if(fwrite("\n", sizeof(char), 1, keyfile) != 1) {
            SAH_TRACEZ_ERROR(ME, "authentication instance: %s unable to write newline to %s: %s", amxd_object_get_name(instance, AMXD_OBJECT_NAMED), keyPath, strerror(errno));
            fclose(keyfile);
            goto err;
        }

        fd = fileno(keyfile);

        if(fchown(fd, uid, gid) == -1) {
            SAH_TRACEZ_ERROR(ME, "Failed to set owner and group to key file");
            fclose(keyfile);
            goto err;
        }

        if(fchmod(fd, S_IRUSR | S_IWUSR) == -1) {
            SAH_TRACEZ_ERROR(ME, "Failed to set permissions to key file");
            fclose(keyfile);
            goto err;
        }

        fflush(keyfile);
        fclose(keyfile);
        keyfile = NULL;
        addAuthKeyFile(keyPath);
err:
        free(userPath);
        free(user);
        free(key);
        free(keyPath);
    }
exit:
    SAH_TRACEZ_INFO(ME, "retval authkey: %d", retval);
    return retval;
}

void ssh_handle_authorized_keys_changed(amxd_object_t* ssh_obj) {
    SAH_TRACEZ_INFO(ME, "ssh_handle_authorized_keys_changed");
    when_null(ssh_obj, exit);
    clean_authkeys_files();
    add_server_authkeys_to_file(ssh_obj);
exit:
    return;
}
