# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.6.26 - 2025-01-06(07:57:16 +0000)

### Other

- [SSH] The SSH key authentication keeps asking for a password  when utilizing the user ssh_lan or ssh_wan

## Release v0.6.25 - 2024-11-05(07:53:09 +0000)

### Other

- [SOP][SSH] The SSH key authentication keeps asking for a password

## Release v0.6.24 - 2024-11-04(10:00:46 +0000)

### Other

- [ssh-server] ssh tests failing on sgv2 and ib3 because it fails to start

## Release v0.6.23 - 2024-10-24(10:20:13 +0000)

### Other

- [Generic][TR069] - Not possible to fetch the full datamodel via tr069

## Release v0.6.22 - 2024-09-10(07:09:53 +0000)

### Other

- [AppArmor] Create AppAmor profile for plugins

## Release v0.6.21 - 2024-08-30(10:11:52 +0000)

## Release v0.6.20 - 2024-08-06(08:52:10 +0000)

### Other

- add support for multiple runlevels

## Release v0.6.19 - 2024-07-25(11:14:45 +0000)

### Other

- [ssh-server] Error message is confusing

## Release v0.6.18 - 2024-07-23(07:44:21 +0000)

### Fixes

- Better shutdown script

## Release v0.6.17 - 2024-07-18(14:34:27 +0000)

### Other

- [PRPL][CI] ssh-server: random: SSH.Server.{i}.Session datamodel is missing

## Release v0.6.16 - 2024-07-11(06:48:16 +0000)

### Fixes

- [LOG]The Home CPE SHALL have the ability to log all attempts...

## Release v0.6.15 - 2024-07-11(05:56:43 +0000)

### Other

- Configure dropbear to store config into /etc/config

## Release v0.6.14 - 2024-07-02(11:50:08 +0000)

### Other

- TR-181 SSH data model issue in 00.10.40.

## Release v0.6.13 - 2024-06-10(10:40:52 +0000)

### Other

- [SSH WAN] SSH Server Remote access with PRPL Parameter

## Release v0.6.12 - 2024-05-27(18:59:29 +0000)

### Other

- [SSH] Check UserID instead of Username to decide where to place ssh keys

## Release v0.6.11 - 2024-04-30(11:26:19 +0000)

### Other

- [ssh-server] add support for authorized_keys for user groups

## Release v0.6.10 - 2024-04-26(09:31:03 +0000)

### Other

- [SSH] Fix defaults for wan instance

## Release v0.6.9 - 2024-04-24(15:27:25 +0000)

### Other

- [ssh-server] add support for authorized_keys for user groups

## Release v0.6.8 - 2024-04-18(10:39:19 +0000)

### Other

- Running reset_soft prints reset_hard

## Release v0.6.7 - 2024-04-15(10:31:06 +0000)

## Release v0.6.6 - 2024-04-12(17:21:03 +0000)

### Other

- [PRPL][CI] ssh-server: random: SSH.Server.{i}.Session datamodel is missing

## Release v0.6.5 - 2024-04-11(09:12:01 +0000)

### Other

- [SSH] connection established using users who are not part of the GroupRestriction.

## Release v0.6.4 - 2024-04-10(10:01:56 +0000)

### Changes

- Make amxb timeouts configurable

## Release v0.6.3 - 2024-04-08(15:31:59 +0000)

### Other

- No password required to login using ssh after a reset_hard

## Release v0.6.2 - 2024-04-08(15:25:13 +0000)

### Other

- [SSH] Remove non standard table SSH.Server.{i}.AuthorizedKey...

## Release v0.6.0 - 2024-03-23(13:15:38 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v0.5.20 - 2024-03-20(21:29:42 +0000)

### Other

- - [CHR2fA] SSH connection established using users who are not part of the GroupRestriction.

## Release v0.5.19 - 2024-03-20(06:34:35 +0000)

### Other

- Rework TR-181 interface stacks indexing number

## Release v0.5.18 - 2024-03-14(18:55:01 +0000)

### Other

- [SSH] Stats improvements

## Release v0.5.17 - 2024-03-07(13:54:33 +0000)

### Other

- [SSH] Stats improvements

## Release v0.5.16 - 2024-03-02(08:30:51 +0000)

### Other

- [SSH server] Report connection failure after MaxAuthTries

## Release v0.5.15 - 2024-02-27(22:28:48 +0000)

### Other

- - Add defaults config directory in  ssh-server plugin

## Release v0.5.14 - 2024-02-26(09:45:39 +0000)

### Other

- [SAFRAN_PRPL] [SSH] WAN SSH Enable Parameter is not upgrade persistent

## Release v0.5.13 - 2024-02-23(09:26:11 +0000)

### Other

- [SAFRAN_PRPL] [SSH] WAN SSH Enable Parameter is not upgrade persistent

## Release v0.5.12 - 2024-02-21(16:52:38 +0000)

### Other

- [SAFRAN_PRPL] [SSH] WAN SSH Enable Parameter is not upgrade persistent

## Release v0.5.11 - 2024-02-20(10:27:46 +0000)

## Release v0.5.10 - 2024-02-19(07:47:45 +0000)

### Other

- [SAFRAN_PRPL] [SSH] WAN SSH Enable Parameter is not upgrade persistent

## Release v0.5.9 - 2024-01-26(14:07:44 +0000)

### Other

- [ssh-server][ssh-client] debuginfo option not implemented in init script

## Release v0.5.8 - 2024-01-22(09:07:13 +0000)

### Fixes

- [ProcessMonitor] cleanup plugin

## Release v0.5.7 - 2024-01-16(11:29:21 +0000)

### Other

- [ssh-server] ssh-server stopping when not both IPV4 and IPV6 configured when IPVersion=0

## Release v0.5.6 - 2023-12-23(13:48:56 +0000)

### Fixes

- Fix 'extra_command not found' during ssh-server startup

## Release v0.5.5 - 2023-11-30(16:53:24 +0000)

### Other

- [SSH] Have the possibility to restrict SSH access to one group only

## Release v0.5.4 - 2023-11-23(09:59:51 +0000)

### Other

- Enable IPv6 for WAN SSH

## Release v0.5.3 - 2023-11-19(12:22:33 +0000)

### Other

- [prplOs][SSH-server] Session not accounted for user with blank password (default prplOS)

## Release v0.5.2 - 2023-11-17(15:00:10 +0000)

### Other

- [SSH] Have the possibility to restrict SSH access to one group only

## Release v0.5.1 - 2023-11-16(14:47:02 +0000)

### Other

- [SSH] Have the possibility to restrict SSH access to one group only

## Release v0.5.0 - 2023-11-13(12:37:53 +0000)

### Other

- [SSH] Have the possibility to restrict SSH access to one group only

## Release v0.4.9 - 2023-08-10(08:42:38 +0000)

## Release v0.4.8 - 2023-07-20(13:21:34 +0000)

### Other

- [PRPL][SSH]Allowsourceprefix should not be taken into account when allowallipv4/6 is enabled

## Release v0.4.7 - 2023-07-20(13:17:04 +0000)

### Fixes

- [PRPL][ORANGE POC]SSHD : potential memory leak

## Release v0.4.6 - 2023-07-03(13:26:13 +0000)

### Fixes

- Init script has no shutdown function

## Release v0.4.5 - 2023-06-15(10:27:36 +0000)

### Other

- [BBF][TR-181]Device.SSH Server support (AuthorizedKey on wrong location)

## Release v0.4.4 - 2023-05-02(14:05:32 +0000)

## Release v0.4.3 - 2023-04-17(08:29:42 +0000)

### Fixes

- [odl]Remove deprecated odl keywords

## Release v0.4.2 - 2023-03-17(18:37:03 +0000)

### Other

- [baf] Correct typo in config option

## Release v0.4.1 - 2023-03-16(13:41:18 +0000)

### Other

- Add AP config files

## Release v0.4.0 - 2023-03-10(15:00:17 +0000)

### Other

- [SSH][IPv6] SSH can't connect to IPv6 link-local addresse(LLA) of hgw

## Release v0.3.19 - 2023-02-23(09:53:18 +0000)

### Other

- ssh-server: documentation generation fails

## Release v0.3.18 - 2023-01-12(14:45:11 +0000)

### Fixes

- random: dropbear is not running

## Release v0.3.17 - 2023-01-09(09:23:55 +0000)

### Fixes

- init without clean on local variables

## Release v0.3.16 - 2022-11-15(12:24:04 +0000)

### Other

- [SSH] Connection is dropped on IPv4 whenever there is an IPv6 change on same interface

## Release v0.3.15 - 2022-10-27(10:27:56 +0000)

### Other

- SSH server: regresssion:new baf handling incompatible with ifdef usage

## Release v0.3.14 - 2022-10-13(08:47:14 +0000)

### Other

- Improve plugin boot order

## Release v0.3.13 - 2022-10-13(08:02:49 +0000)

## Release v0.3.12 - 2022-09-27(12:09:35 +0000)

### Other

- [TR181-SSH] ssh instability when performing firstboots

## Release v0.3.11 - 2022-09-22(07:15:19 +0000)

### Other

- [amx][ssh] Too many restarts when Enabling an Ssh instance

## Release v0.3.10 - 2022-09-08(11:55:36 +0000)

### Other

- [amx][ssh-server] Remove the Vendor Extension X_PRPL-COM_ from the SSH root object.

## Release v0.3.9 - 2022-09-08(09:35:00 +0000)

### Other

- [SSHServer] Reduce log levels of some debug logs

## Release v0.3.8 - 2022-08-19(12:15:41 +0000)

### Fixes

- Call dropbear `hk_generate_as_needed` at boot

### Other

- Opensource component

## Release v0.3.7 - 2022-07-29(14:48:34 +0000)

### Fixes

- Wait for Firewall. before dynamically setting firewall rules

## Release v0.3.6 - 2022-06-30(08:24:29 +0000)

### Fixes

- [ssh-server] Remote SSH not working

## Release v0.3.5 - 2022-06-28(06:40:28 +0000)

### Other

- reduce log level of debug statements

## Release v0.3.4 - 2022-06-20(12:36:07 +0000)

### Other

- [PRPLoS] Reduce logging during start up.

## Release v0.3.3 - 2022-06-16(06:47:05 +0000)

### Changes

- Set ssh-server as default

## Release v0.3.2 - 2022-06-15(09:53:53 +0000)

### Fixes

- refused ssh connection after reboot

## Release v0.3.1 - 2022-05-19(12:38:31 +0000)

### Fixes

- [Gitlab CI][Unit tests][valgrind] Pipeline doesn't stop when memory leaks are detected

## Release v0.3.0 - 2022-05-18(07:44:31 +0000)

### Other

- [amx][ssh-server] Integration of SSH Server plugin in prplOs

## Release v0.2.0 - 2022-05-03(13:05:06 +0000)

### Other

- [amx][SSH Server plugin] Integrate amx SSH server on SOP

## Release v0.1.0 - 2022-04-29(08:23:12 +0000)

### Fixes

- [tr181 plugins][makefile] Dangerous clean target for all tr181 components

### Other

- [amx][SSH Server plugin] Integrate amx SSH server on SOP

## Release 0.0.3 - 2021-03-01(18:12:08 +0000)

### Changes

- Update baf license
- Add copybara
- Updates readme - adds note about /proc/<pid>/task/<tid>/children and kernel versions

## Release 0.0.2 - 2021-02-28(19:41:05 +0000)

### New

- Unit tests

### Fixes

- Issues discovered with unit-test

## Release 0.0.1 - 2021-02-27(22:40:53 +0000)

### New

- Initial release
- launch and stop dropbear
- monitor dropbear child processes (sessions)
- data model dropbear configuration  parameters
- data model dropbear status parameters

