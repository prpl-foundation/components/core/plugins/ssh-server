/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <regex.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxo/amxo.h>

#include "ssh_server.h"
#include "dm_ssh_server.h"
#include "test_session_monitor.h"

static amxd_dm_t dm;
static amxo_parser_t parser;
static const char* odl_defs = "ssh_server_test.odl";

static void handle_events(void) {
    printf("Handling events ");
    while(amxp_signal_read() == 0) {
        printf(".");
    }
    printf("\n");
}

static void _handle_events_no_print(void) {
    while(amxp_signal_read() == 0) {
    }
}

static void read_sigalrm(void) {
    sigset_t mask;
    int sfd;
    struct signalfd_siginfo fdsi;
    ssize_t s;

    sigemptyset(&mask);
    sigaddset(&mask, SIGALRM);

    sigprocmask(SIG_BLOCK, &mask, NULL);

    sfd = signalfd(-1, &mask, 0);
    s = read(sfd, &fdsi, sizeof(struct signalfd_siginfo));
    assert_int_equal(s, sizeof(struct signalfd_siginfo));
    if(fdsi.ssi_signo == SIGALRM) {
        printf("Got SIGALRM\n");
    } else {
        printf("Read unexpected signal\n");
    }
}

static void test_timer_callback(UNUSED amxp_timer_t* const timer,
                                void* data) {
    amxp_proc_ctrl_t* proc = (amxp_proc_ctrl_t*) data;
    amxc_var_t pid;
    amxc_var_init(&pid);
    amxc_var_set(uint32_t, &pid, proc->proc->pid);
    amxp_proc_ctrl_stop(proc);
    amxp_sigmngr_emit_signal(NULL, "proc:disable", &pid);

    amxc_var_clean(&pid);
}

static void test_stopped(UNUSED const char* const event_name,
                         UNUSED const amxc_var_t* const event_data,
                         void* const priv) {
    amxp_proc_ctrl_t* proc = (amxp_proc_ctrl_t*) priv;
    amxc_var_t pid;
    amxc_var_init(&pid);

    when_null(proc, leave);

    amxc_var_set(uint32_t, &pid, proc->proc->pid);
    amxp_timer_stop(proc->timer);
    amxp_proc_ctrl_stop_childs(proc);

    amxp_sigmngr_emit_signal(NULL, "proc:stopped", &pid);

leave:
    amxc_var_clean(&pid);
}

static void test_free_char(amxc_array_it_t* it) {
    char* txt = (char*) amxc_array_it_get_data(it);
    free(txt);
}

int __wrap_amxm_so_open(amxm_shared_object_t** so,
                        const char* shared_object_name,
                        const char* const path_to_so);
int __wrap_amxm_close_all(void);
int __wrap_amxm_execute_function(const char* const shared_object_name,
                                 const char* const module_name,
                                 const char* const func_name,
                                 amxc_var_t* args,
                                 amxc_var_t* ret);
bool __wrap_netmodel_initialize(void);
void __wrap_netmodel_cleanup(void);

char dummy_query;

netmodel_query_t* __wrap_netmodel_openQuery_getFirstParameter(const char* intf,
                                                              const char* subscriber,
                                                              const char* flag,
                                                              const char* traverse,
                                                              netmodel_callback_t handler,
                                                              void* userdata);

void __wrap_netmodel_closeQuery(netmodel_query_t* const netmodel_query);

int __wrap_amxp_proc_ctrl_new(amxp_proc_ctrl_t** proc, amxp_proc_ctrl_cmd_t cmd_build_fn);
void __wrap_amxp_proc_ctrl_delete(amxp_proc_ctrl_t** proc);
int __wrap_amxp_proc_ctrl_stop(amxp_proc_ctrl_t* proc);
int __wrap_amxp_proc_ctrl_start(amxp_proc_ctrl_t* proc, uint32_t minutes, amxc_var_t* settings);
int __wrap_amxp_proc_ctrl_get_child_pids(amxp_proc_ctrl_t* proc);

const char* __wrap_ssh_server_get_vendor_prefix();

const char* __wrap_ssh_server_get_vendor_prefix() {
    char* ret = "";
    return ret;
}

int __wrap_amxm_so_open(amxm_shared_object_t** so,
                        const char* shared_object_name,
                        UNUSED const char* const path_to_so) {
    int retval = -1;
    when_null(so, leave);
    when_str_empty(shared_object_name, leave);
    retval = 0;
leave:
    return retval;
}

int __wrap_amxm_close_all(void) {
    return 0;
}

int __wrap_amxm_execute_function(const char* const shared_object_name,
                                 const char* const module_name,
                                 const char* const func_name,
                                 UNUSED amxc_var_t* args,
                                 UNUSED amxc_var_t* ret) {
    int retval = -1;
    when_str_empty(shared_object_name, leave);
    when_str_empty(module_name, leave);
    when_str_empty(func_name, leave);
    retval = 0;
leave:
    return retval;
}

bool __wrap_netmodel_initialize(void) {
    return true;
}

void __wrap_netmodel_cleanup(void) {
    return;
}

netmodel_query_t* __wrap_netmodel_openQuery_getFirstParameter(const char* intf,
                                                              const char* subscriber,
                                                              const char* flag,
                                                              const char* traverse,
                                                              netmodel_callback_t handler,
                                                              void* userdata) {
    when_str_empty(intf, leave);
    when_str_empty(subscriber, leave);
    when_str_empty(flag, leave);
    when_str_empty(traverse, leave);
    when_null(handler, leave);
    when_null(userdata, leave);
    return((netmodel_query_t*) &dummy_query);
leave:
    return NULL;
}

void __wrap_netmodel_closeQuery(UNUSED netmodel_query_t* const netmodel_query) {
    return;
}

static void intf_getfirst_cb_ext(const char* addr, ssh_server_instance_t* ssh_server) {
    amxc_var_t data;
    amxc_var_init(&data);
    amxc_var_set(cstring_t, &data, addr);
    intf_getfirst_cb(NULL,
                     &data,
                     ssh_server);
    amxc_var_clean(&data);
}

int __wrap_amxp_proc_ctrl_new(amxp_proc_ctrl_t** proc, amxp_proc_ctrl_cmd_t cmd_build_fn) {
    int retval = -1;
    when_null(proc, leave);
    when_null(cmd_build_fn, leave);

    *proc = (amxp_proc_ctrl_t*) calloc(1, sizeof(amxp_proc_ctrl_t));
    when_null(*proc, leave);

    when_failed(amxp_subproc_new(&(*proc)->proc), leave);
    when_failed(amxp_timer_new(&(*proc)->timer, test_timer_callback, *proc), leave);
    when_failed(amxc_array_init(&(*proc)->cmd, 10), leave);
    when_failed(amxc_var_init(&(*proc)->child_proc_pids), leave);

    (*proc)->build = cmd_build_fn;
    amxp_slot_connect((*proc)->proc->sigmngr, "stop", NULL, test_stopped, *proc);

    retval = 0;

leave:
    if((retval != 0) &&
       ((proc != NULL) && (*proc != NULL))) {
        amxp_subproc_delete(&(*proc)->proc);
        amxp_timer_delete(&(*proc)->timer);
        amxc_array_clean(&(*proc)->cmd, NULL);
        amxc_var_clean(&(*proc)->child_proc_pids);
        free(*proc);
        *proc = NULL;
    }
    return retval;
}

void __wrap_amxp_proc_ctrl_delete(amxp_proc_ctrl_t** proc) {
    when_null(proc, leave);
    when_null(*proc, leave);

    amxp_slot_disconnect((*proc)->proc->sigmngr, "stop", test_stopped);

    amxp_subproc_delete(&(*proc)->proc);
    amxp_timer_delete(&(*proc)->timer);
    amxc_array_clean(&(*proc)->cmd, test_free_char);
    amxc_var_clean(&(*proc)->child_proc_pids);

    free(*proc);
    *proc = NULL;

leave:
    return;
}

int __wrap_amxp_proc_ctrl_start(amxp_proc_ctrl_t* proc, uint32_t minutes, amxc_var_t* settings) {
    int retval = -1;
    when_null(proc, leave);

    amxc_array_clean(&proc->cmd, test_free_char);
    retval = proc->build(&proc->cmd, settings);
    when_failed(retval, leave);

    if(minutes != 0) {
        amxp_timer_start(proc->timer, minutes * 60 * 1000);
    }

    proc->proc->pid = 1001;
    proc->proc->is_running = true;
    amxc_var_set_type(&proc->child_proc_pids, AMXC_VAR_ID_LIST);
    retval = mock();

leave:
    return retval;
}

int __wrap_amxp_proc_ctrl_stop(amxp_proc_ctrl_t* proc) {
    int rv = mock();
    amxp_timer_stop(proc->timer);
    proc->proc->pid = 0;
    proc->proc->is_running = false;
    return rv;
}

static const char* dropbear_log = "dropbear.log";
static void dropbear_write_log(const char* str) {
    int fd = open(dropbear_log, O_RDWR | O_CREAT | O_TRUNC, 0644);
    assert_int_not_equal(fd, -1);
    assert_int_not_equal(write(fd, str, strlen(str)), -1);
    close(fd);
}

static void trigger_session_handler(ssh_server_instance_t* ssh_server) {
    int fd = open(dropbear_log, O_RDONLY);
    assert_int_not_equal(fd, -1);
    ssh_server_read_subproc(fd, ssh_server);
    close(fd);
    remove(dropbear_log);
}

static void session_start(ssh_server_instance_t* ssh_server, const char* user, const char* ip, uint32_t port, uint32_t pid) {
    amxc_string_t start_output;
    amxc_string_init(&start_output, 0);
    amxc_string_setf(&start_output, "[%d] Mar 08 13:47:24 Password auth succeeded for '%s' from %s:%d\n", pid, user, ip, port);
    char* dump = amxc_string_take_buffer(&start_output);
    dropbear_write_log(dump);
    free(dump);
    amxc_string_clean(&start_output);

    amxc_var_add(uint32_t, &ssh_server->proc_ctrl->child_proc_pids, pid);

    trigger_session_handler(ssh_server);
}

static void session_start_fragmented(ssh_server_instance_t* ssh_server, const char* user, const char* ip, uint32_t port, uint32_t pid) {
    amxc_string_t start_output1;
    amxc_string_t start_output2;
    char* dump1 = NULL;
    char* dump2 = NULL;

    amxc_string_init(&start_output1, 0);
    amxc_string_setf(&start_output1, "[%d] Mar 08 13:47:24 Password auth succeeded for '%s' ", pid, user);
    dump1 = amxc_string_take_buffer(&start_output1);
    dropbear_write_log(dump1);
    free(dump1);
    amxc_string_clean(&start_output1);

    trigger_session_handler(ssh_server); //parse first part of log

    amxc_string_init(&start_output2, 0);
    amxc_string_setf(&start_output2, "from %s:%d\n", ip, port);
    dump2 = amxc_string_take_buffer(&start_output2);
    dropbear_write_log(dump2);
    free(dump2);
    amxc_string_clean(&start_output2);

    amxc_var_add(uint32_t, &ssh_server->proc_ctrl->child_proc_pids, pid);

    trigger_session_handler(ssh_server); //parse second part of log
}

static void session_failed(ssh_server_instance_t* ssh_server, const char* user, const char* ip, uint32_t port, uint32_t pid) {

    amxc_string_t failed_output;
    amxc_string_init(&failed_output, 0);
    amxc_string_setf(&failed_output, "[%d] Mar 08 13:47:24 Bad password attempt for '%s' from %s:%d\n", pid, user, ip, port);
    char* dump = amxc_string_take_buffer(&failed_output);
    dropbear_write_log(dump);
    free(dump);
    amxc_string_clean(&failed_output);

    amxc_var_add(uint32_t, &ssh_server->proc_ctrl->child_proc_pids, pid);

    trigger_session_handler(ssh_server);
}

static void session_stop(ssh_server_instance_t* ssh_server, const char* user, const char* ip, uint32_t port, uint32_t pid) {

    amxc_string_t stop_output;
    amxc_string_init(&stop_output, 0);
    amxc_string_setf(&stop_output, "[%d] Mar 09 14:26:59 Exit (%s) from <%s:%d>: Disconnect received\n", pid, user, ip, port);
    char* dump = amxc_string_take_buffer(&stop_output);
    dropbear_write_log(dump);
    free(dump);
    amxc_string_clean(&stop_output);

    amxc_var_for_each(var, (&ssh_server->proc_ctrl->child_proc_pids)) {
        uint32_t curr_pid = amxc_var_dyncast(uint32_t, var);
        if(pid == curr_pid) {
            amxc_var_delete(&var);
            break;
        }
    }

    trigger_session_handler(ssh_server);
}

int test_ssh_server_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;
    amxp_signal_t* signal = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    assert_int_equal(amxp_signal_new(NULL, &signal, strsignal(SIGCHLD)), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_resolver_ftab_add(&parser, "cleanup_server", AMXO_FUNC(_cleanup_server)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "app_start", AMXO_FUNC(_app_start)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "ssh_server_enable_changed", AMXO_FUNC(_ssh_server_enable_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "ssh_toggle", AMXO_FUNC(_ssh_toggle)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "ssh_server_added", AMXO_FUNC(_ssh_server_added)), 0);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);

    _ssh_server_main(0, &dm, &parser);

    handle_events();

    return 0;
}

int test_ssh_server_teardown(UNUSED void** state) {
    _ssh_server_main(1, &dm, &parser);

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    return 0;
}

void test_ssh_monitors_session(UNUSED void** state) {
    amxd_object_t* server = NULL;
    char* status = NULL;

    will_return_always(__wrap_amxp_proc_ctrl_start, 0);
    amxp_sigmngr_trigger_signal(&dm.sigmngr, "app:start", NULL);
    server = amxd_dm_findf(&dm, "SSH.Server.1.");
    assert_non_null(server);
    assert_true(amxd_object_get_value(bool, server, "Enable", NULL));
    status = amxd_object_get_value(cstring_t, server, "State", NULL);

    assert_string_equal(status, "Enabled");
    free(status);

    ssh_server_instance_t* ssh_server = (ssh_server_instance_t*) server->priv;
    intf_getfirst_cb_ext("lan", ssh_server);
    handle_events();
    status = amxd_object_get_value(cstring_t, server, "State", NULL);
    assert_string_equal(status, "Running");
    free(status);

    print_message("wait sig alrm");
    read_sigalrm();
    amxp_timers_calculate();
    amxp_timers_check();

    session_start(ssh_server, "user", "192.168.1.100", 6666, 1111);
    session_start(ssh_server, "dre", "fd35:2e30:4632:0:e6c6:7348:3946:257f", 6667, 1112);
    session_start_fragmented(ssh_server, "root", "192.168.1.55", 6668, 1113);
    handle_events();

    amxd_object_t* stats_obj = amxd_object_get_child(server, "Stats");
    const uint32_t successCounter = amxd_object_get_value(uint32_t, stats_obj, "NumberOfSuccessAttempts", NULL);
    assert_non_null(stats_obj);
    assert_int_equal(successCounter, 3);

    amxd_object_t* sessions_obj = amxd_object_get_child(server, "Session");
    assert_non_null(sessions_obj);
    int nbsessions = 0;
    amxd_object_t* session1;
    amxd_object_t* session2;
    amxd_object_t* session3;

    amxd_object_for_each(instance, child_it, sessions_obj) {
        amxd_object_t* instance_obj = amxc_llist_it_get_data(child_it, amxd_object_t, it);
        const uint32_t pid = amxd_object_get_value(uint32_t, instance_obj, "PID", NULL);
        printf("PID = %d\n", pid);

        if(nbsessions == 0) {
            session1 = instance_obj;
        } else if(nbsessions == 1) {
            session2 = instance_obj;
        } else if(nbsessions == 2) {
            session3 = instance_obj;
        }

        nbsessions++;
    }

    assert_int_equal(nbsessions, 3);

    // Session 1
    assert_non_null(session1);
    uint32_t dat = amxd_object_get_value(uint32_t, session1, "PID", NULL);
    assert_int_equal(dat, 1111);

    dat = amxd_object_get_value(uint32_t, session1, "Port", NULL);
    assert_int_equal(dat, 6666);

    char* str = amxd_object_get_value(cstring_t, session1, "User", NULL);
    assert_string_equal(str, "user");
    free(str);
    str = amxd_object_get_value(cstring_t, session1, "IPAddress", NULL);
    assert_string_equal(str, "192.168.1.100");
    free(str);

    // Session 2
    assert_non_null(session2);
    dat = amxd_object_get_value(uint32_t, session2, "PID", NULL);
    assert_int_equal(dat, 1112);

    dat = amxd_object_get_value(uint32_t, session2, "Port", NULL);
    assert_int_equal(dat, 6667);

    str = amxd_object_get_value(cstring_t, session2, "User", NULL);
    assert_string_equal(str, "dre");
    free(str);
    str = amxd_object_get_value(cstring_t, session2, "IPAddress", NULL);
    assert_string_equal(str, "fd35:2e30:4632:0:e6c6:7348:3946:257f");
    free(str);

    // Session 3
    assert_non_null(session3);
    dat = amxd_object_get_value(uint32_t, session3, "PID", NULL);
    assert_int_equal(dat, 1113);

    dat = amxd_object_get_value(uint32_t, session3, "Port", NULL);
    assert_int_equal(dat, 6668);

    str = amxd_object_get_value(cstring_t, session3, "User", NULL);
    assert_string_equal(str, "root");
    free(str);
    str = amxd_object_get_value(cstring_t, session3, "IPAddress", NULL);
    assert_string_equal(str, "192.168.1.55");
    free(str);


    assert_int_equal(amxd_object_get_value(uint32_t, server, "SessionNumberOfEntries", NULL), 3);
    session1 = NULL;
    session_stop(ssh_server, "user", "192.168.1.100", 6666, 1111);
    handle_events();
    nbsessions = 0;
    amxd_object_for_each(instance, child_it, sessions_obj) {
        amxd_object_t* instance_obj = amxc_llist_it_get_data(child_it, amxd_object_t, it);
        const uint32_t pid = amxd_object_get_value(uint32_t, instance_obj, "PID", NULL);
        printf("PID = %d\n", pid);

        nbsessions++;
    }

    assert_int_equal(nbsessions, 2);
    assert_int_equal(amxd_object_get_value(uint32_t, server, "SessionNumberOfEntries", NULL), 2);

    read_sigalrm();
    amxp_timers_calculate();
    amxp_timers_check();
}

void test_ssh_monitors_failed_attempt(UNUSED void** state) {
    amxd_object_t* server = NULL;
    char* status = NULL;
    amxc_var_t params;
    amxc_var_t params_server;

    will_return_always(__wrap_amxp_proc_ctrl_start, 0);
    amxp_sigmngr_trigger_signal(&dm.sigmngr, "app:start", NULL);
    server = amxd_dm_findf(&dm, "SSH.Server.1.");
    assert_non_null(server);

    amxc_var_init(&params_server);
    amxd_object_get_params(server, &params_server, amxd_dm_access_protected);
    amxc_var_dump(&params_server, STDOUT_FILENO);
    amxc_var_clean(&params_server);

    assert_true(amxd_object_get_value(bool, server, "Enable", NULL));
    status = amxd_object_get_value(cstring_t, server, "State", NULL);

    assert_string_equal(status, "Enabled");
    free(status);

    ssh_server_instance_t* ssh_server = (ssh_server_instance_t*) server->priv;
    intf_getfirst_cb_ext("lan", ssh_server);
    handle_events();
    status = amxd_object_get_value(cstring_t, server, "State", NULL);
    assert_string_equal(status, "Running");
    free(status);

    print_message("wait sig alrm");
    read_sigalrm();
    amxp_timers_calculate();
    amxp_timers_check();

    session_start(ssh_server, "user", "192.168.1.100", 6666, 1111);
    session_failed(ssh_server, "user", "192.168.1.100", 6666, 1111);
    session_failed(ssh_server, "user", "192.168.1.100", 6666, 1111);
    handle_events();

    amxd_object_t* FailedAttempt_obj = amxd_object_get_child(server, "FailedAttempt");
    amxd_object_t* stats_obj = amxd_object_get_child(server, "Stats");
    const uint32_t failCounter = amxd_object_get_value(uint32_t, stats_obj, "NumberOfFailedAttempts", NULL);

    assert_non_null(FailedAttempt_obj);
    assert_non_null(stats_obj);

    assert_int_equal(failCounter, 2);

    amxc_var_init(&params);
    amxd_object_get_params(FailedAttempt_obj, &params, amxd_dm_access_protected);
    amxc_var_dump(&params, STDOUT_FILENO);
    amxc_var_clean(&params);

    int nbFailedAttempt = 0;
    amxd_object_t* failedAttempt;
    amxd_object_t* failedAttempt2;

    amxd_object_for_each(instance, child_it, FailedAttempt_obj) {
        amxd_object_t* instance_obj = amxc_llist_it_get_data(child_it, amxd_object_t, it);
        const uint32_t pid = amxd_object_get_value(uint32_t, instance_obj, "PID", NULL);
        printf("PID = %d\n", pid);

        if(nbFailedAttempt == 0) {
            failedAttempt = instance_obj;
        } else if(nbFailedAttempt == 1) {
            failedAttempt2 = instance_obj;
        }

        nbFailedAttempt++;
    }

    assert_int_equal(nbFailedAttempt, 2);

    // Failed Attempt 1
    assert_non_null(failedAttempt);
    uint32_t dat = amxd_object_get_value(uint32_t, failedAttempt, "PID", NULL);
    assert_int_equal(dat, 1111);

    dat = amxd_object_get_value(uint32_t, failedAttempt, "Port", NULL);
    assert_int_equal(dat, 6666);

    char* str = amxd_object_get_value(cstring_t, failedAttempt, "User", NULL);
    assert_string_equal(str, "user");
    free(str);
    str = amxd_object_get_value(cstring_t, failedAttempt, "IPAddress", NULL);
    assert_string_equal(str, "192.168.1.100");
    free(str);

    // Failed Attempt 2
    assert_non_null(failedAttempt2);
    dat = amxd_object_get_value(uint32_t, failedAttempt2, "PID", NULL);
    assert_int_equal(dat, 1111);

    dat = amxd_object_get_value(uint32_t, failedAttempt2, "Port", NULL);
    assert_int_equal(dat, 6666);

    str = amxd_object_get_value(cstring_t, failedAttempt2, "User", NULL);
    assert_string_equal(str, "user");
    free(str);
    str = amxd_object_get_value(cstring_t, failedAttempt2, "IPAddress", NULL);
    assert_string_equal(str, "192.168.1.100");
    free(str);

    assert_int_equal(amxd_object_get_value(uint32_t, server, "FailedAttemptNumberOfEntries", NULL), 2);

    // test rotation
    for(int i = 0; i < 12; i++) {
        session_failed(ssh_server, "user", "192.168.1.100", 6666, 1111);
    }

    assert_int_equal(amxd_object_get_value(uint32_t, server, "FailedAttemptNumberOfEntries", NULL), MAX_FAILED_ATTEMPTS);

    failedAttempt = NULL;
    session_stop(ssh_server, "user", "192.168.1.100", 6666, 1111);
    handle_events();

    read_sigalrm();
    amxp_timers_calculate();
    amxp_timers_check();
}

static void regex_no_match_non_start_end_lines(regex_t* regex, int nmatch) {
    int ret;
    char* line;
    regmatch_t pmatch[nmatch];

    line = "[1060012] Apr 27 14:03:34 Pubkey auth attempt with unknown algo for 'user' from fd22:33ee:e479::729:45190";
    ret = regexec(regex, line, nmatch, pmatch, 0);
    assert_int_equal(ret, REG_NOMATCH);

    line = "[1059994] Apr 27 14:03:27 Not backgrounding";
    ret = regexec(regex, line, nmatch, pmatch, 0);
    assert_int_equal(ret, REG_NOMATCH);

    line = "[1054190] Apr 27 13:29:37 Failed loading /etc/dropbear/dropbear_ed25519_host_key";
    ret = regexec(regex, line, nmatch, pmatch, 0);
    assert_int_equal(ret, REG_NOMATCH);

    line = "[5632] Apr 27 11:13:34 Binding to interface 'br-lan'";
    ret = regexec(regex, line, nmatch, pmatch, 0);
    assert_int_equal(ret, REG_NOMATCH);

    line = "[5636] Apr 27 11:14:00 Child connection from 192.168.1.100:45736";
    ret = regexec(regex, line, nmatch, pmatch, 0);
    assert_int_equal(ret, REG_NOMATCH);

    line = "[1059997] Apr 27 14:03:31 Child connection from fd22:33ee:e479::729:45186";
    ret = regexec(regex, line, nmatch, pmatch, 0);
    assert_int_equal(ret, REG_NOMATCH);

    line = "[5636] Apr 27 11:14:00 Exit before auth: Exited normally";
    ret = regexec(regex, line, nmatch, pmatch, 0);
    assert_int_equal(ret, REG_NOMATCH);

    line = "[6071] Apr 27 12:11:12 Bad password attempt for 'root' from 192.168.1.100:53768";
    ret = regexec(regex, line, nmatch, pmatch, 0);
    assert_int_equal(ret, REG_NOMATCH);

    line = "[6071] Apr 27 12:11:12 Exit before auth (user 'root', 3 fails): Exited normally";
    ret = regexec(regex, line, nmatch, pmatch, 0);
    assert_int_equal(ret, REG_NOMATCH);

    line = "[698654] Apr 27 12:16:47 Early exit: Terminated by signal";
    ret = regexec(regex, line, nmatch, pmatch, 0);
    assert_int_equal(ret, REG_NOMATCH);
}

void test_regex_parsing_start_of_dropbear_session(UNUSED void** state) {
    int ret;
    char* line;
    int nmatch = 5;
    regmatch_t pmatch[nmatch];
    regex_t* session_conn_ok_regex = ssh_server_get_session_ok_regex();

    line = "[5637] Apr 27 11:14:11 Password auth succeeded for 'root' from 192.168.1.100:45750";
    ret = regexec(session_conn_ok_regex, line, nmatch, pmatch, 0);
    assert_int_not_equal(ret, REG_NOMATCH);

    // Begin of session for dropbear 2019
    line = "[878564] Apr 27 12:33:44 Pubkey auth succeeded for 'user' with key sha1!! c3:91:d3:3f:e9:74:18:ee:ed:f4:b1:43:c7:f0:02:e4:5e:9b:41:86 from 127.0.0.1:50846";
    ret = regexec(session_conn_ok_regex, line, nmatch, pmatch, 0);
    assert_int_not_equal(ret, REG_NOMATCH);

    // Begin of session for dropbear 2022
    line = "[1055222] Apr 27 14:00:10 Pubkey auth succeeded for 'user' with ecdsa-sha2-nistp256 key SHA256:TToWSMyqhYLatBFDwt7xI6HBVCnB44R8RaQWU+t959E from fd22:33ee:e479::729:48936";
    ret = regexec(session_conn_ok_regex, line, nmatch, pmatch, 0);
    assert_int_not_equal(ret, REG_NOMATCH);

    line = "[1055244] Apr 27 14:00:25 Pubkey auth succeeded for 'user' with ssh-rsa key SHA256:Wq7/TOVSZZB78qBpNhsdJUJx8NhZ6iZHQbbLKUK0AH0 from fd22:33ee:e479::729:50672";
    ret = regexec(session_conn_ok_regex, line, nmatch, pmatch, 0);
    assert_int_not_equal(ret, REG_NOMATCH);

    line = "[1055204] Apr 27 14:00:00 Pubkey auth succeeded for 'user' with ssh-ed25519 key SHA256:UNwIf9oACnxdkqMrtJIXM2Dyy4FM1jCC0sb6vkvr898 from fd22:33ee:e479::729:57092";
    ret = regexec(session_conn_ok_regex, line, nmatch, pmatch, 0);
    assert_int_not_equal(ret, REG_NOMATCH);

    // SHOULD NOT MATCH:
    // End of session for 2019
    line = "[878564] Apr 27 12:33:56 Exit (admin): Terminated by signal";
    ret = regexec(session_conn_ok_regex, line, nmatch, pmatch, 0);
    assert_int_equal(ret, REG_NOMATCH);

    line = "[693578] Apr 27 11:08:26 Exit (holmes): Disconnect received\n";
    ret = regexec(session_conn_ok_regex, line, nmatch, pmatch, 0);
    assert_int_equal(ret, REG_NOMATCH);

    // End of session for 2022
    line = "[38203] Mar 09 14:26:59 Exit (sah) from <192.168.68.68:6666>: Disconnect received\n";
    ret = regexec(session_conn_ok_regex, line, nmatch, pmatch, 0);
    assert_int_equal(ret, REG_NOMATCH);

    line = "[38203] Mar 09 14:26:59 Exit (bot) from <192.168.68.68:6666>: Idle timeout";
    ret = regexec(session_conn_ok_regex, line, nmatch, pmatch, 0);
    assert_int_equal(ret, REG_NOMATCH);

    line = "[38203] Mar 09 14:26:59 Exit (mozes) from <fd35:2e30:4632:0:e6c6:7348:3946:257f:6666>: Idle timeout";
    ret = regexec(session_conn_ok_regex, line, nmatch, pmatch, 0);
    assert_int_equal(ret, REG_NOMATCH);

    regex_no_match_non_start_end_lines(session_conn_ok_regex, nmatch);
}

void test_regex_parsing_failed_attempt(UNUSED void** state) {
    int ret;
    char* line;
    char* pid_str;
    char* user;
    int nmatch = 3;
    regmatch_t pmatch[nmatch];
    regex_t* session_failed_attempt_regex = ssh_server_get_failed_attempt_regex();

    // The following regex also extracts port and ip for 2022
    // "^\\[([0-9]+)\\].*[Bb]ad password attempt.*'(.*)'.*from ([a-fA-F0-9:\\.]+):([0-9]+)\n{0,1}$";

    // End of session for 2019
    line = "[14455] Jan 31 05:02:18 Bad password attempt for 'sherlock' from 192.168.1.100:34796";
    ret = regexec(session_failed_attempt_regex, line, nmatch, pmatch, 0);
    assert_int_not_equal(ret, REG_NOMATCH);

    line = "[693578] Apr 27 11:08:26 Bad password attempt for 'holmes' from 192.168.255.255:74839";
    ret = regexec(session_failed_attempt_regex, line, nmatch, pmatch, 0);
    assert_int_not_equal(ret, REG_NOMATCH);

    // Check group
    pid_str = strndup(line + pmatch[1].rm_so,
                      pmatch[1].rm_eo - pmatch[1].rm_so);
    user = strndup(line + pmatch[2].rm_so,
                   pmatch[2].rm_eo - pmatch[2].rm_so);
    assert_string_equal(pid_str, "693578");
    assert_string_equal(user, "holmes");
    free(pid_str);
    free(user);

    // SHOULD NOT MATCH:
    line = "[5637] Apr 27 11:14:11 Password auth succeeded for 'root' from 192.168.1.100:45750";
    ret = regexec(session_failed_attempt_regex, line, nmatch, pmatch, 0);
    assert_int_equal(ret, REG_NOMATCH);

    // Begin of session for dropbear 2019
    line = "[878564] Apr 27 12:33:44 Pubkey auth succeeded for 'user' with key sha1!! c3:91:d3:3f:e9:74:18:ee:ed:f4:b1:43:c7:f0:02:e4:5e:9b:41:86 from 127.0.0.1:50846";
    ret = regexec(session_failed_attempt_regex, line, nmatch, pmatch, 0);
    assert_int_equal(ret, REG_NOMATCH);
}

void test_regex_parsing_end_of_dropbear_session(UNUSED void** state) {
    int ret;
    char* line;
    char* pid_str;
    char* user;
    int nmatch = 3;
    regmatch_t pmatch[nmatch];
    regex_t* session_conn_end_regex = ssh_server_get_session_end_regex();

    // The following regex also extracts port and ip for 2022
    // "^\\[([0-9]+)\\].*Exit \\((.*)\\) from <([a-fA-F0-9:\\.]+):([0-9]+)>.*\n{0,1}$";

    // End of session for 2019
    line = "[878564] Apr 27 12:33:56 Exit (admin): Terminated by signal";
    ret = regexec(session_conn_end_regex, line, nmatch, pmatch, 0);
    assert_int_not_equal(ret, REG_NOMATCH);

    line = "[878601] Apr 27 12:34:28 Exit (sahbot): Idle timeout";
    ret = regexec(session_conn_end_regex, line, nmatch, pmatch, 0);
    assert_int_not_equal(ret, REG_NOMATCH);

    line = "[693578] Apr 27 11:08:26 Exit (sherlock): Disconnect received";
    ret = regexec(session_conn_end_regex, line, nmatch, pmatch, 0);
    assert_int_not_equal(ret, REG_NOMATCH);

    line = "[693578] Apr 27 11:08:26 Exit (holmes): Disconnect received\n";
    ret = regexec(session_conn_end_regex, line, nmatch, pmatch, 0);
    assert_int_not_equal(ret, REG_NOMATCH);
    // Check group
    pid_str = strndup(line + pmatch[1].rm_so,
                      pmatch[1].rm_eo - pmatch[1].rm_so);
    user = strndup(line + pmatch[2].rm_so,
                   pmatch[2].rm_eo - pmatch[2].rm_so);
    assert_string_equal(pid_str, "693578");
    assert_string_equal(user, "holmes");
    free(pid_str);
    free(user);

    // End of session for 2022
    line = "[38203] Mar 09 14:26:59 Exit (sah) from <192.168.68.68:6666>: Disconnect received\n";
    ret = regexec(session_conn_end_regex, line, nmatch, pmatch, 0);
    assert_int_not_equal(ret, REG_NOMATCH);
    // Check group
    pid_str = strndup(line + pmatch[1].rm_so,
                      pmatch[1].rm_eo - pmatch[1].rm_so);
    user = strndup(line + pmatch[2].rm_so,
                   pmatch[2].rm_eo - pmatch[2].rm_so);
    assert_string_equal(pid_str, "38203");
    assert_string_equal(user, "sah");
    free(pid_str);
    free(user);

    line = "[38203] Mar 09 14:26:59 Exit (bot) from <192.168.68.68:6666>: Idle timeout";
    ret = regexec(session_conn_end_regex, line, nmatch, pmatch, 0);
    assert_int_not_equal(ret, REG_NOMATCH);

    line = "[38203] Mar 09 14:26:59 Exit (mozes) from <fd35:2e30:4632:0:e6c6:7348:3946:257f:6666>: Idle timeout";
    ret = regexec(session_conn_end_regex, line, nmatch, pmatch, 0);
    assert_int_not_equal(ret, REG_NOMATCH);

    // SHOULD NOT MATCH:
    line = "[5637] Apr 27 11:14:11 Password auth succeeded for 'root' from 192.168.1.100:45750";
    ret = regexec(session_conn_end_regex, line, nmatch, pmatch, 0);
    assert_int_equal(ret, REG_NOMATCH);

    // Begin of session for dropbear 2019
    line = "[878564] Apr 27 12:33:44 Pubkey auth succeeded for 'user' with key sha1!! c3:91:d3:3f:e9:74:18:ee:ed:f4:b1:43:c7:f0:02:e4:5e:9b:41:86 from 127.0.0.1:50846";
    ret = regexec(session_conn_end_regex, line, nmatch, pmatch, 0);
    assert_int_equal(ret, REG_NOMATCH);

    // Begin of session for dropbear 2022
    line = "[1055222] Apr 27 14:00:10 Pubkey auth succeeded for 'user' with ecdsa-sha2-nistp256 key SHA256:TToWSMyqhYLatBFDwt7xI6HBVCnB44R8RaQWU+t959E from fd22:33ee:e479::729:48936";
    ret = regexec(session_conn_end_regex, line, nmatch, pmatch, 0);
    assert_int_equal(ret, REG_NOMATCH);

    regex_no_match_non_start_end_lines(session_conn_end_regex, nmatch);
}

void test_ssh_monitors_stress_test(UNUSED void** state) {
    amxd_object_t* server = NULL;
    char* status = NULL;
    int i = 0;
    int NB_SESSIONS = 100;
    will_return_always(__wrap_amxp_proc_ctrl_start, 0);
    amxp_sigmngr_trigger_signal(&dm.sigmngr, "app:start", NULL);
    server = amxd_dm_findf(&dm, "SSH.Server.1.");
    assert_non_null(server);
    assert_true(amxd_object_get_value(bool, server, "Enable", NULL));
    status = amxd_object_get_value(cstring_t, server, "State", NULL);

    assert_string_equal(status, "Enabled");
    free(status);

    ssh_server_instance_t* ssh_server = (ssh_server_instance_t*) server->priv;
    intf_getfirst_cb_ext("lan", ssh_server);
    handle_events();
    status = amxd_object_get_value(cstring_t, server, "State", NULL);
    assert_string_equal(status, "Running");
    free(status);

    print_message("wait sig alrm");
    read_sigalrm();
    amxp_timers_calculate();
    amxp_timers_check();

    for(i = 0; i < NB_SESSIONS; i++) {
        session_start(ssh_server, "user", "192.168.1.100", 9999 + i, 26666 + i);
        _handle_events_no_print();
    }

    for(i = 0; i < NB_SESSIONS; i++) {
        session_stop(ssh_server, "user", "192.168.1.100", 9999 + i, 26666 + i);
        _handle_events_no_print();
    }

    read_sigalrm();
    amxp_timers_calculate();
    amxp_timers_check();
}