/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <sys/stat.h>
#include <regex.h>
#include <unistd.h>
#include <stddef.h>
#include <sys/types.h>

#include <netmodel/client.h>
#include <netmodel/common_api.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <debug/user_trace.h>

#include "dm_ssh_server.h"

#define min(a, b)             \
    ({                           \
        __typeof__ (a) _a = (a); \
        __typeof__ (b) _b = (b); \
        _a < _b ? _a : _b;       \
    })

static int ssh_server_initialize(amxd_object_t* templ, amxd_object_t* instance, void* priv);

static int ssh_server_query_ssh_group(amxd_object_t* server) {
    SAH_TRACEZ_INFO(ME, "ssh_server_query_ssh_group");
    amxb_bus_ctx_t* bus_ctx = amxb_be_who_has("Device.Users.Group.");
    ssh_server_instance_t* ssh_server = NULL;
    amxc_string_t templ_str;
    amxc_var_t res;
    char* group_path = NULL;
    const char* ssh_group = NULL;
    int rv = -1;

    amxc_string_init(&templ_str, 0);
    amxc_var_init(&res);

    when_null_trace(bus_ctx, exit, ERROR, "No bus_ctx");

    ssh_server = (ssh_server_instance_t*) server->priv;
    when_null_trace(ssh_server, exit, ERROR, "No ssh server");

    amxc_string_setf(&templ_str, "%sGroupRestriction", ssh_server_get_vendor_prefix());
    group_path = amxd_object_get_value(cstring_t, server, amxc_string_get(&templ_str, 0), NULL);

    // When group_path is empty, no query is necessary
    when_str_empty(group_path, noquery);

    // We are not querying a parameter and mod-dmext makes sure there is no dot
    // Reuse previous string
    amxc_string_setf(&templ_str, "%s.Groupname", group_path);
    rv = amxb_get(bus_ctx, amxc_string_get(&templ_str, 0), 0, &res, 5);
    when_failed_trace(rv, exit, ERROR, "Failed to get SSH group '%s' from bus: %d", group_path, rv);

    // [{ path = { Groupname = "group" } }]
    ssh_group = GET_CHAR(GETI_ARG(GETI_ARG(&res, 0), 0), "Groupname");
    when_str_empty_trace(ssh_group, exit, ERROR, "Empty parameter Groupname");

noquery:
    rv = 0;
    // delete previous group
    free(ssh_server->ssh_group);
    ssh_server->ssh_group = NULL;

    if(ssh_group != NULL) {
        ssh_server->ssh_group = strdup(ssh_group);
    }

exit:
    free(group_path);
    amxc_string_clean(&templ_str);
    amxc_var_clean(&res);
    return rv;
}

static int ssh_server_start(UNUSED amxd_object_t* templ,
                            amxd_object_t* server,
                            UNUSED void* priv) {
    SAH_TRACEZ_INFO(ME, "ssh_server_start: %s", amxd_object_get_name(server, AMXD_OBJECT_NAMED));
    int retval = -1;
    uint32_t time = 0;
    char* status = NULL;
    ssh_server_instance_t* ssh_server = NULL;
    amxp_proc_ctrl_t* dropbear_ctrl = NULL;
    amxc_var_t settings;

    when_null(server, exit);
    /* enable firewall rules */
    ssh_server_enable_fw_services(server); // best effort, assume the firewall rule will be created
    ssh_server_query_ssh_group(server);

    ssh_server = (ssh_server_instance_t*) server->priv;
    dropbear_ctrl = (amxp_proc_ctrl_t*) ssh_server->proc_ctrl;
    time = amxd_object_get_value(uint32_t, server, "AutoDisableDuration", NULL);
    status = amxd_object_get_value(cstring_t, server, "State", NULL);
    when_null(status, exit);
    amxc_var_init(&settings);
    ssh_server_fetch_settings(server, &settings);
    SAH_TRACEZ_INFO(ME, "status: %s", status);
    if(strcmp(status, "Running") ||
       (!strcmp(status, "Running") && (ssh_server->proc_ctrl_stopping == true))) {
        if(ssh_server->proc_ctrl_stopping == false) {
            SAH_TRACEZ_INFO(ME, "start dropbear");
            retval = amxp_proc_ctrl_start(dropbear_ctrl, time, &settings);
            if(retval == 0) {
                ssh_server_update_internal_state(server, (void*) "Running");
                ssh_server->pid = amxp_subproc_get_pid(dropbear_ctrl->proc);
                ssh_server_start_monitor();
            } else {
                ssh_server_disable_fw_services(server);
                ssh_server_update_internal_state(server, (void*) "Error");
            }
        } else {
            ssh_server->proc_ctrl_restart = true;
        }
    }
    free(status);
    amxc_var_clean(&settings);
exit:
    return retval;
}

static amxd_object_t* ssh_server_get_stats_obj(amxd_object_t* server) {
    amxd_object_t* statsObj = NULL;
    amxc_string_t templ_str;
    when_null(server, exit);
    amxc_string_init(&templ_str, 0);
    amxc_string_setf(&templ_str, "%sStats", ssh_server_get_vendor_prefix());
    statsObj = amxd_object_get_child(server, amxc_string_get(&templ_str, 0));
    amxc_string_clean(&templ_str);
exit:
    return statsObj;
}

static int ssh_server_inc_stat(amxd_object_t* server, bool success) {
    amxd_trans_t transaction;
    int retval = 0;
    amxd_object_t* statsObj = ssh_server_get_stats_obj(server);

    when_null(statsObj, exit);

    amxd_trans_init(&transaction);
    amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&transaction, statsObj);

    if(success) {
        uint32_t counter = amxd_object_get_uint32_t(statsObj, "NumberOfSuccessAttempts", NULL);
        uint32_t counterSinceActivation = amxd_object_get_uint32_t(statsObj, "NumberOfSuccessAttemptsSinceActivation", NULL);
        amxd_trans_set_value(uint32_t, &transaction, "NumberOfSuccessAttempts", ++counter);
        amxd_trans_set_value(uint32_t, &transaction, "NumberOfSuccessAttemptsSinceActivation", ++counterSinceActivation);
    } else {
        uint32_t counter = amxd_object_get_uint32_t(statsObj, "NumberOfFailedAttempts", NULL);
        uint32_t counterSinceActivation = amxd_object_get_uint32_t(statsObj, "NumberOfFailedAttemptsSinceActivation", NULL);
        amxd_trans_set_value(uint32_t, &transaction, "NumberOfFailedAttempts", ++counter);
        amxd_trans_set_value(uint32_t, &transaction, "NumberOfFailedAttemptsSinceActivation", ++counterSinceActivation);
    }

    retval = amxd_trans_apply(&transaction, ssh_server_get_dm());
    amxd_trans_clean(&transaction);

exit:
    if(retval != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Update of Stats instance failed: %d", retval);
    }
    return retval;
}

static int ssh_server_reset_stats(amxd_object_t* server) {
    amxd_trans_t transaction;
    int retval = 0;
    amxd_object_t* statsObj = ssh_server_get_stats_obj(server);

    when_null(statsObj, exit);

    amxd_trans_init(&transaction);
    amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&transaction, statsObj);

    amxd_trans_set_value(uint32_t, &transaction, "NumberOfFailedAttemptsSinceActivation", 0);
    amxd_trans_set_value(uint32_t, &transaction, "NumberOfSuccessAttemptsSinceActivation", 0);

    retval = amxd_trans_apply(&transaction, ssh_server_get_dm());
    amxd_trans_clean(&transaction);

exit:
    if(retval != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Update of Stats instance failed: %d", retval);
    }

    return retval;
}

static int ssh_server_enable(UNUSED amxd_object_t* templ,
                             amxd_object_t* server,
                             UNUSED void* priv) {
    SAH_TRACEZ_INFO(ME, "ssh_server_enable");
    int retval = -1;
    char* intf = NULL;

    when_null(server, exit);
    ssh_server_update_internal_state(server, (void*) "Enabled");

    ssh_server_instance_t* ssh_server = (ssh_server_instance_t*) server->priv;
    if(ssh_server) {
        intf = amxd_object_get_value(cstring_t, server, "Interface", NULL);
        when_str_empty(intf, error);
        retval = ssh_server_open_netmodel_queries(ssh_server, intf,
                                                  intf_getfirst_cb);
        when_failed(retval, error);
        free(intf);
    } else {
        bool is_enabled = true;
        retval = ssh_server_initialize(NULL, server, &is_enabled);
        when_failed(retval, error);
    }
    ssh_server_reset_stats(server);
    return retval;
error:
    free(intf);
    retval = -1;
    ssh_server_update_internal_state(server, (void*) "Error");
exit:
    return retval;
}

static int ssh_server_stop(UNUSED amxd_object_t* templ,
                           amxd_object_t* server,
                           UNUSED void* priv) {
    SAH_TRACEZ_INFO(ME, "ssh_server_stop");

    int retval = -1;
    const char* status = NULL;
    ssh_server_instance_t* ssh_server = NULL;

    when_null(server, exit);
    ssh_server = (ssh_server_instance_t*) server->priv;
    status = (const char*) priv;
    ssh_server_disable_fw_services(server);
    if(ssh_server) {
        ssh_server->proc_ctrl_restart = false;
        if(ssh_server->pid) {
            SAH_TRACEZ_INFO(ME, "ssh_server_stop ctrl");
            amxp_proc_ctrl_t* dropbear_ctrl = (amxp_proc_ctrl_t*) ssh_server->proc_ctrl;
            ssh_server->proc_ctrl_stopping = true;
            ssh_server->pid = amxp_subproc_get_pid(dropbear_ctrl->proc);
            retval = amxp_proc_ctrl_stop(dropbear_ctrl);
        } else {
            retval = 0;
        }
    }
    if((retval == 0) && status && *status) {
        ssh_server_update_internal_state(server, (void*) status);
    }
exit:
    return retval;
}

static int ssh_server_disable(UNUSED amxd_object_t* templ,
                              amxd_object_t* server,
                              UNUSED void* priv) {
    SAH_TRACEZ_INFO(ME, "ssh server disable");

    int retval = -1;
    const char* status = NULL;
    ssh_server_instance_t* ssh_server = NULL;
    when_null(server, exit);
    status = (const char*) priv;
    ssh_server = (ssh_server_instance_t*) server->priv;

    if(ssh_server) {
        ssh_server_close_netmodel_queries(ssh_server);
        retval = ssh_server_stop(NULL, server, NULL);
    }
    if((retval == 0) && status && *status) {
        ssh_server_update_internal_state(server, status);
    }
exit:
    return retval;
}

static void ssh_server_proc_disable(UNUSED const char* const event_name,
                                    const amxc_var_t* const event_data,
                                    UNUSED void* const priv) {

    /* proc disable received after Activation Timeout */
    SAH_TRACEZ_INFO(ME, "proc disable");
    uint32_t pid = 0;
    amxd_dm_t* dm = NULL;
    amxd_object_t* server = NULL;
    const char* status = "Disabled";

    when_null(event_data, exit);
    pid = amxc_var_dyncast(uint32_t, event_data);
    dm = ssh_server_get_dm();
    SAH_TRACEZ_INFO(ME, "proc disabled: %d", pid);
    server = amxd_dm_findf(dm, "SSH.Server.[ PID == %d ].", pid);
    if(server != NULL) {
        ssh_server_update_internal_state(server, (void*) status);
        ssh_server_instance_t* ssh_server = (ssh_server_instance_t*) server->priv;
        ssh_server->proc_ctrl_stopping = false;
        ssh_server->proc_ctrl_restart = false;
        ssh_server->pid = 0;
    }
exit:
    return;
}

static void ssh_server_proc_stopped(UNUSED const char* const event_name,
                                    const amxc_var_t* const event_data,
                                    UNUSED void* const priv) {
    SAH_TRACEZ_INFO(ME, "proc stopped");
    uint32_t pid = 0;
    amxd_dm_t* dm = NULL;
    amxd_object_t* server = NULL;
    const char* status = "Stopped";

    when_null(event_data, exit);
    pid = amxc_var_dyncast(uint32_t, event_data);
    dm = ssh_server_get_dm();
    SAH_TRACEZ_INFO(ME, "Dropbear instance stopped: %d", pid);
    server = amxd_dm_findf(dm, "SSH.Server.");
    amxd_object_for_each(instance, child_it, server) {
        amxd_object_t* instance_obj = amxc_llist_it_get_data(child_it, amxd_object_t, it);
        ssh_server_instance_t* ssh_server = (ssh_server_instance_t*) instance_obj->priv;
        if(ssh_server && (ssh_server->pid == pid)) {
            SAH_TRACEZ_INFO(ME, "Running Instance");
            ssh_server->proc_ctrl_stopping = false;
            ssh_server->pid = 0;
            if(!ssh_server->proc_ctrl_restart) {
                ssh_server_update_internal_state(instance_obj, (void*) status);
            } else {
                SAH_TRACEZ_INFO(ME, "restart dropbear for server");
                amxc_var_t settings;
                amxp_proc_ctrl_t* dropbear_ctrl = (amxp_proc_ctrl_t*) ssh_server->proc_ctrl;
                uint32_t time = 0;
                ssh_server->proc_ctrl_restart = false;
                int retval = 0;

                amxc_var_init(&settings);
                ssh_server_fetch_settings(instance_obj, &settings);
                time = amxd_object_get_value(uint32_t, instance_obj, "AutoDisableDuration", NULL);
                retval = amxp_proc_ctrl_start(dropbear_ctrl, time, &settings);
                if(retval == 0) {
                    ssh_server_update_internal_state(instance_obj, (void*) "Running");
                    ssh_server->pid = amxp_subproc_get_pid(dropbear_ctrl->proc);
                    ssh_server_start_monitor();
                } else {
                    ssh_server_disable_fw_services(instance_obj);
                    ssh_server_update_internal_state(instance_obj, (void*) "Error");
                }
                amxc_var_clean(&settings);
            }
        }
    }
exit:
    return;
}

void intf_getfirst_cb(UNUSED const char* sig_name,
                      const amxc_var_t* data,
                      void* priv) {

    SAH_TRACEZ_INFO(ME, "intf_getfirst_cb");
    ssh_server_instance_t* server = NULL;
    bool changed = false;
    amxc_string_t intf_str;
    amxc_string_init(&intf_str, 0);

    when_null(data, exit);
    when_null(priv, exit);
    amxc_string_set(&intf_str, GET_CHAR(data, NULL));


    server = (ssh_server_instance_t*) priv;

    if((amxc_string_is_empty(&server->intf_str) &&
        !amxc_string_is_empty(&intf_str)) ||
       (!amxc_string_is_empty(&server->intf_str) &&
        amxc_string_is_empty(&intf_str)) ||
       (strcmp(amxc_string_get(&intf_str, 0), amxc_string_get(&server->intf_str, 0)))) {
        SAH_TRACEZ_INFO(ME, "old intf: %s new intf: %s",
                        server->intf_str.buffer,
                        intf_str.buffer);
        changed = true;
    }

    if(!changed) {
        goto exit;
    }
    if(!amxc_string_is_empty(&server->intf_str)) {
        ssh_server_stop(NULL, server->object, NULL);
        amxc_string_clean(&server->intf_str);
    }
    if(!amxc_string_is_empty(&intf_str)) {
        amxc_string_copy(&server->intf_str, &intf_str);
        ssh_server_start(NULL, server->object, NULL);
    }
    SAH_TRACEZ_INFO(ME, "server intf str: %s", server->intf_str.buffer);
exit:
    amxc_string_clean(&intf_str);
    return;
}

static void ssh_server_create_session_entry(amxd_object_t* sessions_obj,
                                            uint32_t pid,
                                            const char* user,
                                            const char* ipaddr,
                                            uint32_t port) {
    when_null(sessions_obj, exit);
    when_null(user, exit);
    when_null(ipaddr, exit);

    // Insert into dm
    amxd_trans_t transaction;
    int retval = 0;

    amxd_trans_init(&transaction);
    amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&transaction, sessions_obj);

    amxd_trans_add_inst(&transaction, 0, NULL);
    amxd_trans_set_value(cstring_t, &transaction, "User", user);
    amxd_trans_set_value(cstring_t, &transaction, "IPAddress", ipaddr);
    amxd_trans_set_value(uint32_t, &transaction, "Port", port);
    amxd_trans_set_value(uint32_t, &transaction, "PID", pid);

    retval = amxd_trans_apply(&transaction, ssh_server_get_dm());

    if(retval != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Creation of Session instance failed: %d", retval);
    }
    amxd_trans_clean(&transaction);

exit:
    return;
}

static void ssh_server_remove_session_entry(amxd_object_t* sessions_obj,
                                            uint32_t pid,
                                            const char* user) {

    when_null(sessions_obj, exit);
    when_null(user, exit);

    amxd_trans_t transaction;
    int retval = 0;

    amxd_object_t* instance_obj = amxd_object_findf(sessions_obj,
                                                    "[User=='%s' && "
                                                    " PID==%u].",
                                                    user,
                                                    pid);
    // Gives null pointer when multiple instances are found
    when_null(instance_obj, exit);

    amxd_trans_init(&transaction);
    amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&transaction, sessions_obj);

    amxd_trans_del_inst(&transaction, amxd_object_get_index(instance_obj), NULL);

    retval = amxd_trans_apply(&transaction, ssh_server_get_dm());

    if(retval != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Deletion of Session instance failed: %d", retval);
    }
    amxd_trans_clean(&transaction);

    return;

exit:
    SAH_TRACEZ_ERROR(ME, "Could not find Session to remove: User='%s',"
                     "PID=%d",
                     user,
                     pid);
}

static bool ssh_server_dropbear_parse_session_start(const char* output, ssh_server_instance_t* ssh_server) {
    char* pid_str;
    char* user;
    char* ipaddr;
    char* port_str;
    uint32_t port;
    uint32_t pid;
    int nmatch = 5; // pid, user, ipaddr, port
    int ret;
    regmatch_t pmatch[nmatch];
    amxc_var_t cast_uint;

    amxd_object_t* session_obj = amxd_object_get_child(ssh_server->object, "Session");
    ret = regexec(ssh_server_get_session_ok_regex(), output, nmatch, pmatch, 0);

    if((ret == REG_NOMATCH) || (NULL == session_obj)) {
        return false;
    }

    pid_str = strndup(output + pmatch[1].rm_so,
                      pmatch[1].rm_eo - pmatch[1].rm_so);
    user = strndup(output + pmatch[2].rm_so,
                   pmatch[2].rm_eo - pmatch[2].rm_so);
    ipaddr = strndup(output + pmatch[3].rm_so,
                     pmatch[3].rm_eo - pmatch[3].rm_so);
    port_str = strndup(output + pmatch[4].rm_so,
                       pmatch[4].rm_eo - pmatch[4].rm_so);

    USER_TRACE_WARNING(TRACE_CAT_SYSTEM, "[Authentication][SSH][SUCCESS] User='%s'", user);

    SAH_TRACEZ_INFO(ME, "Parsed PID='%s',"
                    "User='%s',"
                    "IPAddress='%s' "
                    "and Port='%s'",
                    pid_str,
                    user,
                    ipaddr,
                    port_str);

    // Convert str to integers
    amxc_var_init(&cast_uint);
    amxc_var_set(cstring_t, &cast_uint, pid_str);
    pid = GET_UINT32(&cast_uint, NULL);
    amxc_var_set(cstring_t, &cast_uint, port_str);
    port = GET_UINT32(&cast_uint, NULL);
    amxc_var_clean(&cast_uint);

    ssh_server_create_session_entry(session_obj, pid, user, ipaddr, port);
    ssh_server_inc_stat(ssh_server->object, true);

    free(pid_str);
    free(user);
    free(ipaddr);
    free(port_str);

    return true;
}

static bool ssh_server_dropbear_parse_session_end(const char* output, ssh_server_instance_t* ssh_server) {
    char* pid_str;
    char* user;
    uint32_t pid;
    int nmatch = 3; // pid, user
    int ret;
    regmatch_t pmatch[nmatch];
    amxc_var_t cast_uint;
    amxd_object_t* session_obj = amxd_object_get_child(ssh_server->object, "Session");
    ret = regexec(ssh_server_get_session_end_regex(), output, nmatch, pmatch, 0);

    if((ret == REG_NOMATCH) || (NULL == session_obj)) {
        return false;
    }

    pid_str = strndup(output + pmatch[1].rm_so,
                      pmatch[1].rm_eo - pmatch[1].rm_so);
    user = strndup(output + pmatch[2].rm_so,
                   pmatch[2].rm_eo - pmatch[2].rm_so);

    SAH_TRACEZ_INFO(ME, "Parsed PID='%s',"
                    "User='%s'",
                    pid_str,
                    user);

    // Convert str to integers
    amxc_var_init(&cast_uint);
    amxc_var_set(cstring_t, &cast_uint, pid_str);
    pid = GET_UINT32(&cast_uint, NULL);
    amxc_var_clean(&cast_uint);

    ssh_server_remove_session_entry(session_obj, pid, user);

    free(pid_str);
    free(user);

    return true;
}

static void ssh_server_create_attempt_entry(amxd_object_t* attempts_obj,
                                            uint32_t pid,
                                            const char* user,
                                            const char* ipaddr,
                                            uint32_t port) {
    when_null(attempts_obj, exit);
    when_null(user, exit);
    when_null(ipaddr, exit);

    // Insert into dm
    amxc_ts_t ts;
    amxd_trans_t transaction;
    int retval = 0;

    amxc_ts_now(&ts);
    amxd_trans_init(&transaction);
    amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&transaction, attempts_obj);

    amxd_trans_add_inst(&transaction, 0, NULL);
    amxd_trans_set_value(cstring_t, &transaction, "User", user);
    amxd_trans_set_value(cstring_t, &transaction, "IPAddress", ipaddr);
    amxd_trans_set_value(uint32_t, &transaction, "Port", port);
    amxd_trans_set_value(uint32_t, &transaction, "PID", pid);
    amxd_trans_set_value(amxc_ts_t, &transaction, "Time", &ts);

    retval = amxd_trans_apply(&transaction, ssh_server_get_dm());

    if(retval != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Creation of Session instance failed: %d", retval);
    }
    amxd_trans_clean(&transaction);

exit:
    return;
}

static void ssh_server_remove_oldest_failed_entry(amxd_object_t* attempts_obj) {
    when_null(attempts_obj, exit);
    amxd_trans_t transaction;
    int retval = 0;
    amxc_ts_t* oldest_time = NULL;
    bool first = true;
    amxd_object_t* instance_obj = NULL;
    amxd_object_for_each(instance, it, attempts_obj) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        if(first) {
            first = false;
            instance_obj = amxc_container_of(it, amxd_object_t, it);
            oldest_time = amxd_object_get_value(amxc_ts_t, instance, "Time", NULL);
        } else {
            amxc_ts_t* time_ts = amxd_object_get_value(amxc_ts_t, instance, "Time", NULL);
            if(amxc_ts_compare(time_ts, oldest_time) == -1) {
                free(oldest_time);
                instance_obj = amxc_container_of(it, amxd_object_t, it);
                oldest_time = amxd_object_get_value(amxc_ts_t, instance, "Time", NULL);
            }
            free(time_ts);
        }
    }
    free(oldest_time);

    // Gives null pointer when multiple instances are found
    when_null(instance_obj, exit);

    amxd_trans_init(&transaction);
    amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&transaction, attempts_obj);

    amxd_trans_del_inst(&transaction, amxd_object_get_index(instance_obj), NULL);

    retval = amxd_trans_apply(&transaction, ssh_server_get_dm());

    if(retval != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Deletion of Session instance failed: %d", retval);
    }
    amxd_trans_clean(&transaction);

    retval = 1;
exit:
    if(retval == 0) {
        SAH_TRACEZ_ERROR(ME, "Could not find attempt to remove");
    }
    return;
}

static bool ssh_server_dropbear_parse_failed_attempt(const char* output, ssh_server_instance_t* ssh_server) {
    char* pid_str = NULL;
    char* user = NULL;
    char* ipaddr = NULL;
    char* port_str = NULL;
    uint32_t failedAttempts;
    uint32_t port;
    uint32_t pid;
    int nmatch = 5; // pid, user, ipaddr, port
    bool ret = false;
    regmatch_t pmatch[nmatch];
    amxc_var_t cast_uint;
    amxc_string_t templ_str;

    amxc_string_init(&templ_str, 0);
    amxc_string_setf(&templ_str, "%sFailedAttempt", ssh_server_get_vendor_prefix());

    amxd_object_t* attempts_obj = amxd_object_get_child(ssh_server->object, amxc_string_get(&templ_str, 0));

    amxc_string_clean(&templ_str);

    if((regexec(ssh_server_get_failed_attempt_regex(), output, nmatch, pmatch, 0) == REG_NOMATCH) || (NULL == attempts_obj)) {
        goto exit;
    }

    if((pmatch[1].rm_so == -1) || (pmatch[1].rm_eo == -1) ||
       (pmatch[2].rm_so == -1) || (pmatch[2].rm_eo == -1) ||
       (pmatch[3].rm_so == -1) || (pmatch[3].rm_eo == -1) ||
       (pmatch[4].rm_so == -1) || (pmatch[4].rm_eo == -1)) {
        goto exit;
    }

    pid_str = strndup(output + pmatch[1].rm_so, pmatch[1].rm_eo - pmatch[1].rm_so);
    user = strndup(output + pmatch[2].rm_so, pmatch[2].rm_eo - pmatch[2].rm_so);
    ipaddr = strndup(output + pmatch[3].rm_so, pmatch[3].rm_eo - pmatch[3].rm_so);
    port_str = strndup(output + pmatch[4].rm_so, pmatch[4].rm_eo - pmatch[4].rm_so);

    if((pid_str == NULL) ||
       (user == NULL) ||
       (ipaddr == NULL) ||
       (port_str == NULL)) {
        goto exit;
    }

    USER_TRACE_WARNING(TRACE_CAT_SYSTEM, "[Authentication][SSH][FAILED] User='%s'", user);

    SAH_TRACEZ_INFO(ME, "Parsed PID='%s', User='%s', IPAddress='%s', and Port='%s'",
                    pid_str, user, ipaddr, port_str);

    // Convert str to integers
    amxc_var_init(&cast_uint);
    amxc_var_set(cstring_t, &cast_uint, pid_str);
    pid = GET_UINT32(&cast_uint, NULL);
    amxc_var_set(cstring_t, &cast_uint, port_str);
    port = GET_UINT32(&cast_uint, NULL);
    amxc_var_clean(&cast_uint);

    amxc_string_init(&templ_str, 0);
    amxc_string_setf(&templ_str, "%sFailedAttemptNumberOfEntries", ssh_server_get_vendor_prefix());
    failedAttempts = amxd_object_get_value(uint32_t, ssh_server->object, amxc_string_get(&templ_str, 0), NULL);

    if(failedAttempts == MAX_FAILED_ATTEMPTS) {
        ssh_server_remove_oldest_failed_entry(attempts_obj);
    }

    amxc_string_clean(&templ_str);

    ssh_server_create_attempt_entry(attempts_obj, pid, user, ipaddr, port);
    ssh_server_inc_stat(ssh_server->object, false);

    ret = true;

exit:
    if(pid_str) {
        free(pid_str);
    }
    if(user) {
        free(user);
    }
    if(ipaddr) {
        free(ipaddr);
    }
    if(port_str) {
        free(port_str);
    }

    return ret;
}

void PRIVATE ssh_server_read_subproc(int fd, void* priv) {
    SAH_TRACEZ_INFO(ME, "ssh_server_read_subproc");
    char buffer[1024] = "";
    ssize_t bytesRead = 0;
    ssh_server_instance_t* ssh_server = (ssh_server_instance_t*) priv;

    when_null_trace(ssh_server, exit, ERROR, "SSH server is null");

    int pos = -1;
    char* substr = NULL;

    while(1) {
        // Use a loop to handle EINTR and read data
        while((bytesRead = read(fd, buffer, sizeof(buffer) - 1)) > 0) {
            buffer[bytesRead] = '\0'; // Null-terminate the buffer
            amxc_string_append(&ssh_server->log, buffer, bytesRead);

            // Parse log entries while there are newlines
            while((pos = amxc_string_search(&ssh_server->log, "\n", 0)) != -1) {
                substr = amxc_string_dup(&ssh_server->log, 0, pos + 1);
                if(!ssh_server_dropbear_parse_session_start(substr, ssh_server) &&
                   !ssh_server_dropbear_parse_session_end(substr, ssh_server) &&
                   !ssh_server_dropbear_parse_failed_attempt(substr, ssh_server)) {
                    SAH_TRACEZ_INFO(ME, "Dropbear output did not match with regex");
                }
                amxc_string_remove_at(&ssh_server->log, 0, pos + 1);
                free(substr);
                substr = NULL;
            }
            // we don't reset the ssh_server->log as there might be data left that does not contain newline yet...
        }

        if(bytesRead == 0) {
            // End of file
            break;
        } else if(bytesRead == -1) {
            // Handle read errors
            if(errno == EINTR) {
                // Interrupted by a signal, retry the read
                continue;
            } else if(errno == EAGAIN) {
                // No data available to read
                break;
            } else {
                SAH_TRACEZ_ERROR(ME, "Error reading data: %s (%d)", strerror(errno), errno);
                break;
            }
        }
    }

exit:
    return;
}

static int ssh_server_create(ssh_server_instance_t** ssh_server,
                             amxd_object_t* instance) {
    SAH_TRACEZ_INFO(ME, "ssh_server_create");
    int retval = -1;
    int fd = -1;
    *ssh_server = calloc(1, sizeof(ssh_server_instance_t));
    when_null(*ssh_server, exit);
    amxc_string_init(&(*ssh_server)->intf_str, 0);
    amxc_string_init(&(*ssh_server)->log, 0);
    (*ssh_server)->object = instance;
    instance->priv = *ssh_server;

    retval = amxp_proc_ctrl_new(&(*ssh_server)->proc_ctrl, dropbear_ctrl_build_cmd);
    when_failed_trace(retval, exit, ERROR, "Can not create server control");
    // Monitor output of dropbear process
    fd = amxp_subproc_open_fd((*ssh_server)->proc_ctrl->proc, STDERR_FILENO);
    if(fd != -1) {
        amxo_connection_add(ssh_server_get_parser(),
                            fd,
                            ssh_server_read_subproc,
                            NULL,
                            AMXO_CUSTOM,
                            *ssh_server);
    }
exit:
    return retval;
}

int ssh_server_initialize(UNUSED amxd_object_t* templ,
                          amxd_object_t* instance,
                          void* priv) {
    SAH_TRACEZ_INFO(ME, "ssh_server_initialize instance:%s",
                    amxd_object_get_name(instance, AMXD_OBJECT_NAMED));
    int retval = -1;
    bool* is_enabled = NULL;
    bool instance_enabled = false;
    ssh_server_instance_t* ssh_server = NULL;

    when_null(instance, exit1);
    when_null(priv, exit1);

    if(instance->priv == NULL) {
        retval = ssh_server_create(&ssh_server, instance);
        when_failed(retval, error);
    } else {
        ssh_server = instance->priv;
    }
    // Make sure this query is before the callback from netmodel
    ssh_server_query_ssh_group(instance);
    is_enabled = (bool*) priv;
    instance_enabled = amxd_object_get_value(bool, instance, "Enable", NULL);
    retval = 0;
    if(*is_enabled && instance_enabled) {
        SAH_TRACEZ_INFO(ME, "enable instance");
        char* intf = amxd_object_get_value(cstring_t, instance,
                                           "Interface", NULL);
        when_str_empty_trace(intf, exit2, ERROR,
                             "try to enable ssh instance %s with empty interface",
                             amxd_object_get_name(instance, AMXD_OBJECT_NAMED));
        SAH_TRACEZ_INFO(ME, "open queries on intf: %s", intf);
        ssh_server_update_internal_state(instance, (void*) "Enabled");
        retval = ssh_server_open_netmodel_queries(ssh_server, intf,
                                                  intf_getfirst_cb);
exit2:
        free(intf);
    }
    if(retval == 0) {
        return retval;
    }
error:
    ssh_server_close_netmodel_queries(ssh_server);
    ssh_server_update_internal_state(instance, (void*) "Error");
exit1:
    return retval;
}

void _app_start(UNUSED const char* const event_name,
                UNUSED const amxc_var_t* const event_data,
                UNUSED void* const priv) {
    SAH_TRACEZ_INFO(ME, "app_start");
    amxd_dm_t* dm = ssh_server_get_dm();
    amxd_object_t* ssh = amxd_dm_findf(dm, "SSH");
    bool is_enabled = amxd_object_get_value(bool, ssh, "Enable", NULL);
    bool start = true;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxp_sigmngr_add_signal(NULL, "proc:disable");
    amxp_sigmngr_add_signal(NULL, "proc:stopped");
    amxp_slot_connect(NULL, "proc:disable", NULL, ssh_server_proc_disable, NULL);
    amxp_slot_connect(NULL, "proc:stopped", NULL, ssh_server_proc_stopped, NULL);
    _ssh_server_authorized_keys_changed(NULL, NULL, &start);
    amxd_object_t* servers = amxd_object_get_child(ssh, "Server");
    amxd_object_for_all(servers, "*", ssh_server_initialize, &is_enabled);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void _ssh_toggle(UNUSED const char* const event_name,
                 const amxc_var_t* const event_data,
                 UNUSED void* const priv) {
    SAH_TRACEZ_INFO(ME, "ssh_toggle");
    amxd_object_t* ssh = NULL;

    when_null(event_data, exit);
    ssh = amxd_dm_signal_get_object(ssh_server_get_dm(), event_data);
    if(ssh) {
        bool enable = GETP_BOOL(event_data, "parameters.Enable.to");
        amxd_instance_cb_t fn = ssh_server_stop;
        const char* filter = "[State != 'Stopped'].";
        const char* status = "Stopped";
        amxd_object_t* servers = NULL;
        if(enable) {
            fn = ssh_server_enable;
            filter = "[Enable == true].";
            status = NULL;
            ssh_update_status(ssh, "Enabled");
        } else {
            ssh_update_status(ssh, "Disabled");
        }
        servers = amxd_object_get_child(ssh, "Server");
        amxd_object_for_all(servers, filter, fn, (void*) status);
    }
exit:
    return;
}

void _ssh_server_added(UNUSED const char* const event_name,
                       const amxc_var_t* const event_data,
                       UNUSED void* const priv) {
    SAH_TRACEZ_INFO(ME, "_ssh_server_added");
    amxd_object_t* ssh_servers = NULL;

    when_null(event_data, exit);
    ssh_servers = amxd_dm_signal_get_object(ssh_server_get_dm(), event_data);
    if(ssh_servers) {
        amxd_object_t* ssh = amxd_object_get_parent(ssh_servers);
        amxd_object_t* server = NULL;
        uint32_t index = GET_UINT32(event_data, "index");
        bool is_enabled = amxd_object_get_value(bool, ssh, "Enable", NULL);
        server = amxd_object_get_instance(ssh_servers, NULL, index);
        if(server != NULL) {
            ssh_server_instance_t* ssh_server = (ssh_server_instance_t*) server->priv;
            if(!ssh_server) {
                ssh_server_initialize(ssh_servers, server, &is_enabled);
            }
        }
    }
exit:
    return;
}

void _ssh_server_enable_changed(UNUSED const char* const event_name,
                                const amxc_var_t* const event_data,
                                UNUSED void* const priv) {
    SAH_TRACEZ_INFO(ME, "ssh_server_enable_changed");
    amxd_object_t* server = NULL;

    when_null(event_data, exit);
    server = amxd_dm_signal_get_object(ssh_server_get_dm(), event_data);
    if(server) {
        amxd_object_t* ssh_servers = amxd_object_get_parent(server);
        amxd_object_t* ssh = amxd_object_get_parent(ssh_servers);
        bool server_is_enabled = GETP_BOOL(event_data, "parameters.Enable.to");
        bool ssh_is_enabled = amxd_object_get_value(bool, ssh, "Enable", NULL);
        if(ssh_is_enabled) {
            if(server_is_enabled) {
                SAH_TRACEZ_INFO(ME, "server enable");
                ssh_server_enable(NULL, server, NULL);
            } else {
                SAH_TRACEZ_INFO(ME, "server disable");
                ssh_server_disable(NULL, server, (void*) "Disabled");
            }
        }
    }
exit:
    return;
}

void _ssh_server_duration_changed(UNUSED const char* const event_name,
                                  const amxc_var_t* const event_data,
                                  UNUSED void* const priv) {
    SAH_TRACEZ_INFO(ME, "ssh_server_duration_changed");
    amxd_object_t* server = NULL;
    when_null(event_data, exit);
    server = amxd_dm_signal_get_object(ssh_server_get_dm(), event_data);
    if(server) {
        uint32_t time = GETP_UINT32(event_data, "parameters.AutoDisableDuration.to");
        ssh_server_instance_t* ssh_server = (ssh_server_instance_t*) server->priv;
        SAH_TRACEZ_INFO(ME, "ssh_server_duration_changed: duration: %d", time);
        if(ssh_server) {
            amxp_proc_ctrl_t* dropbear_ctrl = ssh_server->proc_ctrl;
            if(dropbear_ctrl) {
                amxp_proc_ctrl_set_active_duration(dropbear_ctrl, time);
            }
        }
    }
exit:
    return;
}
void _ssh_server_interface_changed(UNUSED const char* const event_name,
                                   const amxc_var_t* const event_data,
                                   UNUSED void* const priv) {
    SAH_TRACEZ_INFO(ME, "ssh_server_interface_changed");
    amxc_var_t* config = NULL;

    when_null(event_data, exit);
    config = ssh_server_get_config();
    if(GETP_BOOL(config, "ssh-server.auto-restart")) {
        amxd_object_t* server = amxd_dm_signal_get_object(ssh_server_get_dm(), event_data);
        if(server) {
            amxd_object_t* ssh_servers = amxd_object_get_parent(server);
            amxd_object_t* ssh = amxd_object_get_parent(ssh_servers);
            bool ssh_is_enabled = amxd_object_get_value(bool, ssh, "Enable", NULL);
            bool server_enable = amxd_object_get_value(bool, server, "Enable", NULL);
            if(ssh_is_enabled) {
                if(server_enable) {
                    SAH_TRACEZ_INFO(ME, "server enable");
                    char* status = amxd_object_get_value(cstring_t, server, "State", NULL);
                    if(strcmp(status, "Disabled")) {
                        ssh_server_disable(NULL, server, NULL);
                    }
                    ssh_server_enable(NULL, server, NULL);
                    free(status);
                }
            }
        }
    }
exit:
    return;
}

void _ssh_server_ssh_group_changed(UNUSED const char* const event_name,
                                   const amxc_var_t* const event_data,
                                   UNUSED void* const priv) {
    SAH_TRACEZ_INFO(ME, "_ssh_server_ssh_group_changed");
    amxd_object_t* server = NULL;

    when_null_trace(event_data, exit, ERROR, "NULL argument event_data");

    server = amxd_dm_signal_get_object(ssh_server_get_dm(), event_data);
    when_null_trace(server, exit, ERROR, "No server");

    when_failed(ssh_server_query_ssh_group(server), exit);
    _ssh_server_settings_changed(NULL, event_data, NULL);
exit:
    return;
}

void _ssh_server_settings_changed(UNUSED const char* const event_name,
                                  const amxc_var_t* const event_data,
                                  UNUSED void* const priv) {
    SAH_TRACEZ_INFO(ME, "ssh_server_settings_changed");
    amxc_var_t* config = NULL;

    when_null(event_data, exit);
    config = ssh_server_get_config();
    if(GETP_BOOL(config, "ssh-server.auto-restart")) {
        amxd_object_t* server = amxd_dm_signal_get_object(ssh_server_get_dm(), event_data);
        if(server != NULL) {
            amxd_object_t* ssh_servers = amxd_object_get_parent(server);
            amxd_object_t* ssh = amxd_object_get_parent(ssh_servers);
            bool ssh_is_enabled = amxd_object_get_value(bool, ssh, "Enable", NULL);
            char* status = amxd_object_get_value(cstring_t, server, "State", NULL);
            bool server_enable = amxd_object_get_value(bool, server, "Enable", NULL);
            if(ssh_is_enabled) {
                if(server_enable) {
                    SAH_TRACEZ_INFO(ME, "server enable");
                    if((strcmp(status, "Running") == 0)) {
                        ssh_server_stop(NULL, server, NULL);
                        ssh_server_start(NULL, server, NULL);
                    } else {
                        if(strcmp(status, "Disabled")) {
                            ssh_server_disable(NULL, server, NULL);
                        }
                        ssh_server_enable(NULL, server, NULL);
                    }
                }
            }
            free(status);
        }
    }
exit:
    return;
}

void _ssh_server_authorized_keys_changed(UNUSED const char* const event_name,
                                         UNUSED const amxc_var_t* const event_data,
                                         void* const priv) {
    SAH_TRACEZ_INFO(ME, "ssh_server_authorized_keys_updated");
    const bool* start = (const bool*) priv;
    amxd_object_t* root = amxd_dm_get_root(ssh_server_get_dm());
    amxd_object_t* obj = NULL;
    amxd_object_t* servers = NULL;

    obj = amxd_object_findf(root, "SSH.");
    servers = amxd_object_findf(root, "SSH.Server.");

    when_null(obj, exit);
    when_null(servers, exit);
    if((start == NULL) || (*start == false)) {
        amxd_object_for_all(servers, "[State != 'Disabled'].", ssh_server_disable, NULL);
    }
    ssh_handle_authorized_keys_changed(obj);
    if((start == NULL) || (*start == false)) {
        amxd_object_for_all(servers, "[Enable == true].", ssh_server_enable, NULL);
    }
exit:
    return;
}
