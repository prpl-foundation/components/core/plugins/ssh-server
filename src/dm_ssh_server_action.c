/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <regex.h>
#include "dm_ssh_server.h"

int ssh_server_cleanup(UNUSED amxd_object_t* templ,
                       amxd_object_t* instance,
                       UNUSED void* priv) {

    amxp_proc_ctrl_t* dropbear_ctrl = NULL;
    ssh_server_instance_t* ssh_server = (ssh_server_instance_t*) instance->priv;
    int proc_fd = -1;
    if(ssh_server) {
        ssh_server_close_netmodel_queries(ssh_server);

        ssh_server_disable_fw_services(instance);

        dropbear_ctrl = (amxp_proc_ctrl_t*) ssh_server->proc_ctrl;
        proc_fd = amxp_subproc_open_fd(dropbear_ctrl->proc, STDERR_FILENO);
        if(proc_fd != -1) {
            amxo_connection_remove(ssh_server_get_parser(), proc_fd);
        }
        amxp_proc_ctrl_delete(&dropbear_ctrl);

        if(ssh_server->ssh_group) {
            free(ssh_server->ssh_group);
            ssh_server->ssh_group = NULL;
        }
        amxc_string_clean(&ssh_server->log);
        free(ssh_server);
        instance->priv = NULL;
    }
    return 0;
}

amxd_status_t _cleanup_server(amxd_object_t* object,
                              UNUSED amxd_param_t* param,
                              amxd_action_t reason,
                              UNUSED const amxc_var_t* const args,
                              amxc_var_t* const retval,
                              UNUSED void* priv) {
    amxd_status_t status = amxd_status_ok;

    if(reason != action_object_destroy) {
        status = amxd_status_function_not_implemented;
        goto leave;
    }

    ssh_server_cleanup(NULL, object, NULL);

    amxc_var_clean(retval);

leave:
    return status;
}
