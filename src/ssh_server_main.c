/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <limits.h>
#include <sys/inotify.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <regex.h>

#include <debug/sahtrace.h>

#include <netmodel/client.h>

#include "dm_ssh_server.h"

typedef struct _ssh_server {
    amxd_dm_t* dm;
    amxo_parser_t* parser;
    amxp_timer_t* timer;
    amxm_shared_object_t* so;
    regex_t session_conn_ok_regex;
    regex_t session_conn_end_regex;
    regex_t failed_attempt_regex;
} ssh_server_t;

static ssh_server_t app;

static int ssh_server_check_sessions(UNUSED amxd_object_t* templ,
                                     amxd_object_t* instance,
                                     UNUSED void* priv) {
    amxd_trans_t transaction;
    int retval = -1;
    size_t num_elemenents = 0;
    ssh_server_instance_t* ssh_server = instance->priv;
    amxp_proc_ctrl_t* dropbear_ctrl = (amxp_proc_ctrl_t*) ssh_server->proc_ctrl;

    if(amxc_var_type_of(&dropbear_ctrl->child_proc_pids) != AMXC_VAR_ID_LIST) {
        return retval;
    }

    amxd_object_t* sessions_obj = amxd_object_get_child(ssh_server->object, "Session");
    when_null(sessions_obj, exit);

    amxd_trans_init(&transaction);
    amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);

    amxd_object_for_each(instance, child_it, sessions_obj) {
        amxd_object_t* instance_obj = amxc_llist_it_get_data(child_it, amxd_object_t, it);
        uint32_t curr_pid = amxd_object_get_value(uint32_t, instance_obj, "PID", NULL);
        bool gotya = false;

        amxc_var_for_each(child_pid, &dropbear_ctrl->child_proc_pids) {
            uint32_t session_pid = amxc_var_dyncast(uint32_t, child_pid);

            if(curr_pid == session_pid) {
                gotya = true;
                break;
            }
        }

        if(!gotya) {
            SAH_TRACEZ_INFO(ME, "Removing non-existing Session with PID %d", curr_pid);
            amxd_trans_select_object(&transaction, sessions_obj);
            amxd_trans_del_inst(&transaction, amxd_object_get_index(instance_obj), NULL);
        }
        num_elemenents++;
    }

    retval = amxd_trans_apply(&transaction, ssh_server_get_dm());
    if(retval != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Deletion of Session instance failed: %d", retval);
    }

    // if not the same amount of members -> probably missed one session to create
    const amxc_llist_t* pids = amxc_var_constcast(amxc_llist_t,
                                                  &dropbear_ctrl->child_proc_pids);
    if((NULL != pids) && (amxc_llist_size(pids) != num_elemenents)) {
        SAH_TRACEZ_ERROR(ME, "Number of sessions '%zu' do not correspond to the "
                         "number of processes '%zu'",
                         num_elemenents,
                         amxc_llist_size(pids));
    }

    amxd_trans_clean(&transaction);
exit:
    return retval;
}

static void ssh_server_check(UNUSED amxp_timer_t* const timer,
                             UNUSED void* data) {
    amxd_object_t* servers = amxd_dm_findf(app.dm, "SSH.Server");
    amxd_object_for_all(servers,
                        "[ State == 'Running' ].",
                        ssh_server_check_sessions,
                        NULL);
}

void ssh_server_start_monitor(void) {
    amxc_var_t* child_mon = GET_ARG(&app.parser->config, "dropbear-child-monitor");
    bool is_enabled = GETP_BOOL(child_mon, "enable");
    uint32_t interval = amxc_var_dyncast(uint32_t, GET_ARG(child_mon, "interval"));

    if(is_enabled) {
        amxp_timer_set_interval(app.timer, interval);

        if(amxp_timer_get_state(app.timer) != amxp_timer_running) {
            amxp_timer_start(app.timer, interval);
        }
    } else {
        amxp_timer_stop(app.timer);
    }
}

amxd_dm_t* ssh_server_get_dm(void) {
    return app.dm;
}

amxc_var_t* ssh_server_get_config(void) {
    return &app.parser->config;
}

regex_t* ssh_server_get_session_ok_regex(void) {
    return &app.session_conn_ok_regex;
}

regex_t* ssh_server_get_session_end_regex(void) {
    return &app.session_conn_end_regex;
}

regex_t* ssh_server_get_failed_attempt_regex(void) {
    return &app.failed_attempt_regex;
}

amxo_parser_t* ssh_server_get_parser(void) {
    return app.parser;
}

const char* ssh_server_get_vendor_prefix(void) {
    amxc_var_t* setting = amxo_parser_get_config(ssh_server_get_parser(), "prefix_");
    const char* prefix = amxc_var_constcast(cstring_t, setting);
    return (prefix != NULL) ? prefix : "";
}

static bool load_module(const char* dir, const char* module, const char* controller) {
    const char* path = NULL;
    amxc_string_t path_str;
    int rv = -1;

    amxc_string_init(&path_str, 0);

    when_str_empty(module, exit);
    when_str_empty(dir, exit);

    amxc_string_setf(&path_str, "%s/%s.so", dir, module);
    path = amxc_string_get(&path_str, 0);

    rv = amxm_so_open(&app.so, controller, path);
    if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to load module %s", path);
        goto exit;
    } else {
        SAH_TRACEZ_INFO(ME, "module %s loaded", path);
    }

exit:
    amxc_string_clean(&path_str);
    return rv == 0;
}

static bool load_firewall_module(UNUSED amxd_object_t* obj) {
    const char* dir = GETP_CHAR(ssh_server_get_config(), "modules.fw-directory");
    return load_module(dir, "mod-fw-amx", "fw");
}

static bool load_modules(amxd_object_t* obj) {
    bool rv = true;
    rv &= load_firewall_module(obj);
    return rv;
}

static void unload_modules(void) {
    app.so = NULL;
    amxm_close_all();
}

static void cleanup_ssh_servers(void) {
    amxd_object_t* root = amxd_dm_get_root(app.dm);
    amxd_object_t* obj = NULL;
    obj = amxd_object_findf(root, "SSH.Server.");
    amxd_object_for_all(obj, ".[Alias != ''].", ssh_server_cleanup, NULL);
}

static int ssh_init(amxd_dm_t* dm, amxo_parser_t* parser) {
    app.dm = dm;
    app.parser = parser;
    netmodel_initialize();
    load_modules(amxd_dm_findf(dm, "SSH"));
    amxp_timer_new(&app.timer, ssh_server_check, NULL);

    // To reuse the regex for successful connection
    const char* regex_conn_ok_str = "^\\[([0-9]+)\\].*[Aa]uth succeeded.*'(.*)'.*from ([a-fA-F0-9:\\.]+):([0-9]+)\n{0,1}$";
    int retval = regcomp(&app.session_conn_ok_regex, regex_conn_ok_str, REG_EXTENDED);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Error compiling regex for session opened: %d", retval);
    }
    // Output for dropbear 2022 also gives port and ip
    const char* regex_conn_end_str = "^\\[([0-9]+)\\].*Exit \\((.*)\\).*:.*\n{0,1}$";
    retval = regcomp(&app.session_conn_end_regex, regex_conn_end_str, REG_EXTENDED);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Error compiling regex for session closed: %d", retval);
    }
    // To reuse the regex for failed connectio attempt
    const char* regex_failed_attempt_str = "^\\[([0-9]+)\\].*[Bb]ad password attempt.*'(.*)'.*from ([a-fA-F0-9:\\.]+):([0-9]+)\n{0,1}$";
    retval = regcomp(&app.failed_attempt_regex, regex_failed_attempt_str, REG_EXTENDED);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Error compiling regex for failed attempt: %d", retval);
    }

    return 0;
}

static int ssh_clean(UNUSED amxd_dm_t* dm, UNUSED amxo_parser_t* parser) {
    amxp_timer_delete(&app.timer);
    cleanup_ssh_servers();
    netmodel_cleanup();
    unload_modules();
    app.dm = NULL;
    app.parser = NULL;
    regfree(&app.session_conn_ok_regex);
    regfree(&app.session_conn_end_regex);
    regfree(&app.failed_attempt_regex);

    return 0;
}


int _ssh_server_main(int reason,
                     amxd_dm_t* dm,
                     amxo_parser_t* parser) {
    int retval = 0;
    switch(reason) {
    case 0:     // START
        retval = ssh_init(dm, parser);
        break;
    case 1:     // STOP
        retval = ssh_clean(dm, parser);
        break;
    }

    return retval;
}
