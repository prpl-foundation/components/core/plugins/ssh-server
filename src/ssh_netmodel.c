/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <sys/stat.h>

#include <netmodel/client.h>
#include <netmodel/common_api.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "dm_ssh_server.h"

void ssh_server_close_netmodel_queries(ssh_server_instance_t* server) {
    SAH_TRACEZ_INFO(ME, "ssh_server close netmodel queries");
    when_null(server, exit);
    netmodel_closeQuery(server->intf_getfirstquery);
    server->intf_getfirstquery = NULL;
    amxc_string_clean(&server->intf_str);
exit:
    return;
}

int ssh_server_open_netmodel_queries(ssh_server_instance_t* ssh_server,
                                     const char* intf,
                                     netmodel_callback_t ip_handler
                                     ) {
    SAH_TRACEZ_INFO(ME, "ssh_server open netmodel queries");
    amxc_string_t intf_dot_str;
    const char* intf_p_dot = NULL;
    int rv = -1;

    when_null(ssh_server, exit);
    when_null(ssh_server->object, exit);
    when_null(intf, exit);

    amxc_string_init(&intf_dot_str, 0);
#ifndef CONFIG_SAH_AMX_SSH_SERVER_SOP
    amxc_string_setf(&intf_dot_str, "%s.", intf);
    amxc_string_replace(&intf_dot_str, "..", ".", UINT32_MAX);
#else
    amxc_string_setf(&intf_dot_str, "%s", intf);
#endif
    intf_p_dot = amxc_string_get(&intf_dot_str, 0);

    if(ssh_server->intf_getfirstquery != NULL) {
        ssh_server_close_netmodel_queries(ssh_server);
    }

    // This function expects a path or reference like 'Device.IP.Interface.3'
    // The dot is required to query and is added here!
    ssh_server->intf_getfirstquery = netmodel_openQuery_getFirstParameter(intf_p_dot,
                                                                          ME,
                                                                          "NetDevName",
                                                                          "netdev-bound",
                                                                          "down",
                                                                          ip_handler,
                                                                          ssh_server);
    rv = 0;
    if(ssh_server->intf_getfirstquery == NULL) {
        SAH_TRACEZ_ERROR(ME, "ssh_server queries failed on server instance: %s",
                         amxd_object_get_name(ssh_server->object, AMXD_OBJECT_NAMED));
        ssh_server_close_netmodel_queries(ssh_server);
        rv = -1;
    }

    amxc_string_clean(&intf_dot_str);
exit:
    return rv;
}

