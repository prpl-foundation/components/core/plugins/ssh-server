#!/bin/sh

. /usr/lib/amx/scripts/amx_init_functions.sh

name="ssh_server"
datamodel_root="SSH"

prepare(){
    if [ ! -d /etc/config/ssh_server ]; then
        mkdir -p /etc/config/ssh_server/odl
    fi
    $(. /etc/init.d/dropbear 2>/dev/null && hk_generate_as_needed)
    if [ -d /etc/dropbear ] && ls /etc/dropbear/*_key >/dev/null 2>&1; then
        for file in /etc/dropbear/*_key; do
            dest="/etc/config/ssh_server/$(basename "$file")"
            if [ ! -f "$dest" ]; then
                mv "$file" "$dest"
            else
                rm "$file"
            fi
        done
    fi
}

case $1 in
    boot)
        prepare
        process_boot ${name} -D
        ;;
    start)
        prepare
        process_start ${name} -D
        ;;
    stop)
        process_stop ${name}
        ;;
    shutdown)
        process_shutdown ${name}
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    debuginfo)
        process_debug_info ${datamodel_root}
        ;;
    *)
        echo "Usage : $0 [start|boot|stop|shutdown|debuginfo|restart]"
        ;;
esac
