/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <limits.h>
#include <sys/inotify.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#include <debug/sahtrace.h>

#include "ssh_server.h"
#include "ssh_server_dropbear_ctrl.h"
#include "dm_ssh_server.h"

int dropbear_ctrl_build_cmd(amxc_array_t* cmd, amxc_var_t* settings) {

    amxc_array_append_data(cmd, strdup("dropbear"));
    amxc_array_append_data(cmd, strdup("-F"));
    // Use stderr instead of default syslog
    amxc_array_append_data(cmd, strdup("-E"));
    amxc_array_append_data(cmd, strdup("-R"));

    if(!GET_BOOL(settings, CFG_ALLOW_PWD_LOGIN)) {
        amxc_array_append_data(cmd, strdup("-s"));
    }
    if(!GET_BOOL(settings, CFG_ALLOW_ROOT_LOGIN)) {
        amxc_array_append_data(cmd, strdup("-w"));
    }
    if(!GET_BOOL(settings, CFG_ALLOW_ROOT_PWD_LOGIN)) {
        amxc_array_append_data(cmd, strdup("-g"));
    }

    {
        uint32_t authtries = GET_UINT32(settings, CFG_MAX_AUTH_TRIES);
        amxc_string_t authtries_cfg;
        amxc_string_init(&authtries_cfg, 0);
        amxc_string_setf(&authtries_cfg, "-T%d", authtries);
        amxc_array_append_data(cmd, amxc_string_take_buffer(&authtries_cfg));
        amxc_string_clean(&authtries_cfg);
    }

    // do not generate hostkeys automatically
    // amxc_array_append_data(cmd, strdup("-R"));

    {
        uint32_t port = GET_UINT32(settings, CFG_PORT);
        amxc_string_t port_cfg;
        amxc_string_init(&port_cfg, 0);
        SAH_TRACEZ_INFO(ME, "port:%d", port);
        amxc_string_setf(&port_cfg, "-p%d", port);
        amxc_array_append_data(cmd, amxc_string_take_buffer(&port_cfg));

        amxc_string_clean(&port_cfg);
    }

    {
        const char* intf = GET_CHAR(settings, CFG_INTERFACE);
        amxc_string_t intf_cfg;
        amxc_string_init(&intf_cfg, 0);
        SAH_TRACEZ_INFO(ME, "interface:%s", intf);
        amxc_string_setf(&intf_cfg, "-l%s", intf);
        amxc_array_append_data(cmd, amxc_string_take_buffer(&intf_cfg));

        amxc_string_clean(&intf_cfg);
    }

// Only in dropbear 2022, not in 2019
    {
        uint32_t keepalive = GET_UINT32(settings, CFG_KEEP_ALIVE);
        amxc_string_t ka_cfg;
        amxc_string_init(&ka_cfg, 0);
        SAH_TRACEZ_INFO(ME, "keepalive:%d", keepalive);
        amxc_string_setf(&ka_cfg, "-K%d", keepalive);
        amxc_array_append_data(cmd, amxc_string_take_buffer(&ka_cfg));

        amxc_string_clean(&ka_cfg);
    }

// Only in dropbear 2022, not in 2019
    {
        uint32_t idle_timeout = GET_UINT32(settings, CFG_IDLE_TIMEOUT);
        amxc_string_t it_cfg;
        amxc_string_init(&it_cfg, 0);
        SAH_TRACEZ_INFO(ME, "idle_timeout:%d", idle_timeout);
        amxc_string_setf(&it_cfg, "-I%d", idle_timeout);
        amxc_array_append_data(cmd, amxc_string_take_buffer(&it_cfg));

        amxc_string_clean(&it_cfg);
    }

    {
        const char* ssh_group = GET_CHAR(settings, CFG_SSH_GROUP);
        if((ssh_group != NULL) && (strlen(ssh_group) > 0)) {
            amxc_string_t ssh_group_cfg;
            amxc_string_init(&ssh_group_cfg, 0);
            SAH_TRACEZ_INFO(ME, "ssh_group:%s", ssh_group);
            amxc_string_setf(&ssh_group_cfg, "-G%s", ssh_group);
            amxc_array_append_data(cmd, amxc_string_take_buffer(&ssh_group_cfg));

            amxc_string_clean(&ssh_group_cfg);
        }
    }

    {
        const char* ssh_hostkey_path = GETP_CHAR(ssh_server_get_config(), "hostkey-path");
        amxc_string_t ssh_hostkey_cfg;
        amxc_string_init(&ssh_hostkey_cfg, 0);
        SAH_TRACEZ_INFO(ME, "rsa hostkey:%s%s", ssh_hostkey_path, CFG_SSH_RSA_HOSTKEY);
        amxc_string_setf(&ssh_hostkey_cfg, "-r%s%s", ssh_hostkey_path, CFG_SSH_RSA_HOSTKEY);
        amxc_array_append_data(cmd, amxc_string_take_buffer(&ssh_hostkey_cfg));

        SAH_TRACEZ_INFO(ME, "ecdsa hostkey:%s%s", ssh_hostkey_path, CFG_SSH_ECDSA_HOSTKEY);
        amxc_string_setf(&ssh_hostkey_cfg, "-r%s%s", ssh_hostkey_path, CFG_SSH_ECDSA_HOSTKEY);
        amxc_array_append_data(cmd, amxc_string_take_buffer(&ssh_hostkey_cfg));

        SAH_TRACEZ_INFO(ME, "ed25519 hostkey:%s%s", ssh_hostkey_path, CFG_SSH_ED25519_HOSTKEY);
        amxc_string_setf(&ssh_hostkey_cfg, "-r%s%s", ssh_hostkey_path, CFG_SSH_ED25519_HOSTKEY);
        amxc_array_append_data(cmd, amxc_string_take_buffer(&ssh_hostkey_cfg));

        amxc_string_clean(&ssh_hostkey_cfg);
    }

    for(uint32_t i = 0; i < amxc_array_capacity(cmd); i++) {
        SAH_TRACEZ_INFO(ME, "dropbear cmd arg: %s",
                        (const char*) amxc_array_it_get_data(amxc_array_get_at(cmd, i)));
    }
    return 0;
}

