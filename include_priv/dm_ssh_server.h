/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__DM_SSH_SERVER_H__)
#define __DM_SSH_SERVER_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "ssh_server.h"

#include "ssh_server_dropbear_ctrl.h"

#include <netmodel/client.h>
#include <netmodel/common_api.h>

#define MAX_FAILED_ATTEMPTS 10

// common helper functions
amxd_dm_t * PRIVATE ssh_server_get_dm(void);
amxc_var_t* PRIVATE ssh_server_get_config(void);
regex_t* PRIVATE ssh_server_get_session_ok_regex(void);
regex_t* PRIVATE ssh_server_get_session_end_regex(void);
regex_t* PRIVATE ssh_server_get_failed_attempt_regex(void);
amxo_parser_t* PRIVATE ssh_server_get_parser(void);
const char* PRIVATE ssh_server_get_vendor_prefix(void);
void PRIVATE ssh_server_start_monitor(void);
int PRIVATE ssh_server_cleanup(amxd_object_t* templ,
                               amxd_object_t* instance,
                               void* priv);
// data model helper functions
int PRIVATE ssh_update_status(amxd_object_t* ssh, const char* status);

void PRIVATE ssh_server_fetch_settings(amxd_object_t* instance, amxc_var_t* settings);
int PRIVATE ssh_server_update_internal_state(amxd_object_t* server, const char* status);
int PRIVATE ssh_server_update_sessions(amxd_object_t* server, uint32_t sessions);
void PRIVATE ssh_server_read_subproc(int fd, void* priv);

int ssh_server_proc_initialize(amxd_object_t* templ,
                               amxd_object_t* instance,
                               void* priv);

// dropbear ctrl event handler
void PRIVATE ssh_server_proc_disabled(const char* const event_name,
                                      const amxc_var_t* const event_data,
                                      void* const priv);

// firewall helper functions

int PRIVATE ssh_server_enable_fw_services(amxd_object_t* server);
int PRIVATE ssh_server_disable_fw_services(amxd_object_t* server);

// key authentication helper functions
void ssh_handle_authorized_keys_changed(amxd_object_t* ssh_server_obj);

// netmodel helper functions

int ssh_server_open_netmodel_queries(ssh_server_instance_t* ssh_server,
                                     const char* intf,
                                     netmodel_callback_t ip_handler
                                     );

void ssh_server_close_netmodel_queries(ssh_server_instance_t* server);

void intf_getfirst_cb(const char* sig_name,
                      const amxc_var_t* data,
                      void* priv);

// entry-point(s)
int _ssh_server_main(int reason, amxd_dm_t* dm, amxo_parser_t* parser);

// data model event handlers

void _app_start(const char* const event_name,
                const amxc_var_t* const event_data,
                void* const priv);

void _app_stop(const char* const event_name,
               const amxc_var_t* const event_data,
               void* const priv);

void _ssh_toggle(const char* const event_name,
                 const amxc_var_t* const event_data,
                 void* const priv);

void _ssh_server_added(const char* const event_name,
                       const amxc_var_t* const event_data,
                       void* const priv);

void _ssh_server_enable_changed(const char* const event_name,
                                const amxc_var_t* const event_data,
                                void* const priv);

void _ssh_server_duration_changed(const char* const event_name,
                                  const amxc_var_t* const event_data,
                                  void* const priv);

void _ssh_server_interface_changed(const char* const event_name,
                                   const amxc_var_t* const event_data,
                                   void* const priv);

void _ssh_server_ssh_group_changed(const char* const event_name,
                                   const amxc_var_t* const event_data,
                                   void* const priv);

void _ssh_server_authorized_keys_changed(const char* const event_name,
                                         const amxc_var_t* const event_data,
                                         void* const priv);

void _ssh_server_settings_changed(const char* const event_name,
                                  const amxc_var_t* const event_data,
                                  void* const priv);

// object actions
amxd_status_t _cleanup_server(amxd_object_t* object,
                              amxd_param_t* param,
                              amxd_action_t reason,
                              const amxc_var_t* const args,
                              amxc_var_t* const retval,
                              void* priv);

// data model methods
amxd_status_t _close_sessions(amxd_object_t* object,
                              amxd_function_t* func,
                              amxc_var_t* args,
                              amxc_var_t* ret);
// for instance of Session
amxd_status_t _Delete(amxd_object_t* object,
                      amxd_function_t* func,
                      amxc_var_t* args,
                      amxc_var_t* ret);

#ifdef __cplusplus
}
#endif

#endif // __DM_SSH_SERVER_H__
