/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxo/amxo.h>

#include "ssh_server.h"
#include "dm_ssh_server.h"
#include "test_start_stop.h"

#define QUERY_IPV4_GETADDR_EXPRESSION "ipv4"
#define QUERY_IPV6_GETADDR_EXPRESSION "ipv6 && global && !tentative && permanent"
#define QUERY_IPV4_AND_IPV6_GETADDR_EXPRESSION "ipv4 || (ipv6 && global && !tentative && permanent)"
#define VENDOR_PREFIX "X_TEST_SUITE_"

static char* _assert_str;
#define assert_str_attr_equal(o, a, e) \
    _assert_str = amxd_object_get_value(cstring_t, o, a, NULL); \
    _assert_string_equal((_assert_str), (e), __FILE__, __LINE__); \
    free(_assert_str);

static uint8_t ipversion = 0;
static amxd_dm_t dm;
static amxo_parser_t parser;
static const char* odl_defs = "ssh_server_test.odl";


static void handle_events(void) {
    printf("Handling events ");
    while(amxp_signal_read() == 0) {
        printf(".");
    }
    printf("\n");
}

static void read_sigalrm(void) {
    sigset_t mask;
    int sfd;
    struct signalfd_siginfo fdsi;
    ssize_t s;

    sigemptyset(&mask);
    sigaddset(&mask, SIGALRM);

    sigprocmask(SIG_BLOCK, &mask, NULL);

    sfd = signalfd(-1, &mask, 0);
    s = read(sfd, &fdsi, sizeof(struct signalfd_siginfo));
    assert_int_equal(s, sizeof(struct signalfd_siginfo));
    if(fdsi.ssi_signo == SIGALRM) {
        printf("Got SIGALRM\n");
    } else {
        printf("Read unexpected signal\n");
    }
}

static void test_timer_callback(UNUSED amxp_timer_t* const timer,
                                void* data) {
    amxp_proc_ctrl_t* proc = (amxp_proc_ctrl_t*) data;
    amxc_var_t pid;
    amxc_var_init(&pid);
    amxc_var_set(uint32_t, &pid, proc->proc->pid);
    amxp_sigmngr_emit_signal(NULL, "proc:disable", &pid);
    amxp_proc_ctrl_stop(proc);
    amxc_var_clean(&pid);
}

static void test_stopped(UNUSED const char* const event_name,
                         UNUSED const amxc_var_t* const event_data,
                         void* const priv) {
    amxp_proc_ctrl_t* proc = (amxp_proc_ctrl_t*) priv;
    amxc_var_t pid;
    amxc_var_init(&pid);

    when_null(proc, leave);

    amxc_var_set(uint32_t, &pid, proc->proc->pid);
    amxp_timer_stop(proc->timer);
    amxp_proc_ctrl_stop_childs(proc);

    amxp_sigmngr_emit_signal(NULL, "proc:stopped", &pid);

leave:
    amxc_var_clean(&pid);
}

static void test_free_char(amxc_array_it_t* it) {
    char* txt = (char*) amxc_array_it_get_data(it);
    free(txt);
}

int __wrap_amxm_so_open(amxm_shared_object_t** so,
                        const char* shared_object_name,
                        const char* const path_to_so);
int __wrap_amxm_close_all(void);
int __wrap_amxm_execute_function(const char* const shared_object_name,
                                 const char* const module_name,
                                 const char* const func_name,
                                 amxc_var_t* args,
                                 amxc_var_t* ret);
bool __wrap_netmodel_initialize(void);
void __wrap_netmodel_cleanup(void);

char dummy_query;

netmodel_query_t* __wrap_netmodel_openQuery_getFirstParameter(const char* intf,
                                                              const char* subscriber,
                                                              const char* flag,
                                                              const char* traverse,
                                                              netmodel_callback_t handler,
                                                              void* userdata);
void __wrap_netmodel_closeQuery(netmodel_query_t* const netmodel_query);


int __wrap_amxp_proc_ctrl_new(amxp_proc_ctrl_t** proc, amxp_proc_ctrl_cmd_t cmd_build_fn);
void __wrap_amxp_proc_ctrl_delete(amxp_proc_ctrl_t** proc);
int __wrap_amxp_proc_ctrl_stop(amxp_proc_ctrl_t* proc);
int __wrap_amxp_proc_ctrl_start(amxp_proc_ctrl_t* proc, uint32_t minutes, amxc_var_t* settings);
bool __wrap_query_ssh_user(char* userPath, char** user);

amxb_bus_ctx_t* __wrap_amxb_be_who_has(const char* object_path);
int __wrap_amxb_get(amxb_bus_ctx_t* const bus_ctx,
                    const char* object,
                    int32_t depth,
                    amxc_var_t* ret,
                    int timeout);

int __wrap_amxm_so_open(amxm_shared_object_t** so,
                        const char* shared_object_name,
                        UNUSED const char* const path_to_so) {
    int retval = -1;
    when_null(so, leave);
    when_str_empty(shared_object_name, leave);
    retval = 0;
leave:
    return retval;
}

int __wrap_amxm_close_all(void) {
    return 0;
}

// FIREWALL CONFIGURATION
// Somehow we need to check the configuration of the firewall.
// Lets store a copy of the arguments in a key-value map to allow
// verification in the tests.
// Initialized in the setup and cleaned in the teardown of this test suite
static amxc_var_t* test_fw_entries = NULL;

int __wrap_amxm_execute_function(const char* const shared_object_name,
                                 const char* const module_name,
                                 const char* const func_name,
                                 amxc_var_t* args,
                                 UNUSED amxc_var_t* ret) {
    printf("amxm_execute_function\n");
    int retval = -1;
    const char* id = NULL;
    amxc_var_t* copy_var = NULL;
    when_str_empty(shared_object_name, leave);
    when_str_empty(module_name, leave);
    when_str_empty(func_name, leave);
    when_null(args, leave); // Delete/add fw entry needs args

    assert_non_null(test_fw_entries);

    if((strcmp(module_name, "fw") == 0) && (strcmp(func_name, "set_service") == 0)) {
        id = GETP_CHAR(args, "id");
        assert_non_null(id);
        copy_var = amxc_var_take_key(test_fw_entries, id);
        if(NULL != copy_var) {
            // printf("Deleting previous %s from cache\n", id);
            amxc_var_delete(&copy_var);
        }

        assert_int_equal(amxc_var_set_key(test_fw_entries, id, args, AMXC_VAR_FLAG_COPY), 0);
    }
    retval = 0;
leave:
    return retval;
}

bool __wrap_netmodel_initialize(void) {
    return true;
}

void __wrap_netmodel_cleanup(void) {
    return;
}

static int check_firewall_ids(ssh_server_instance_t* ssh_server) {
    printf("ipversion: %d ipv6_fw_serviceid: %s ipv4_fw_serviceid: %s\n", ipversion, ssh_server->ipv6_fw_serviceid, ssh_server->ipv4_fw_serviceid);
    switch(ipversion) {
    case 0: {
        if((ssh_server->ipv6_fw_serviceid) &&
           (ssh_server->ipv4_fw_serviceid)) {
            return 0;
        }
        break;
    }
    case 4: {
        if((!ssh_server->ipv6_fw_serviceid) &&
           (ssh_server->ipv4_fw_serviceid)) {
            return 0;
        }
        break;
    }
    case 6: {
        if((!ssh_server->ipv4_fw_serviceid) &&
           (ssh_server->ipv6_fw_serviceid)) {
            return 0;
        }
        break;
    }
    }
    return 1;
}

netmodel_query_t* __wrap_netmodel_openQuery_getFirstParameter(const char* intf,
                                                              const char* subscriber,
                                                              const char* flag,
                                                              const char* traverse,
                                                              netmodel_callback_t handler,
                                                              void* userdata) {
    when_str_empty(intf, leave);
    when_str_empty(subscriber, leave);
    when_str_empty(flag, leave);
    when_str_empty(traverse, leave);
    when_null(handler, leave);
    when_null(userdata, leave);
    return((netmodel_query_t*) &dummy_query);
leave:
    return NULL;
}

void __wrap_netmodel_closeQuery(UNUSED netmodel_query_t* const netmodel_query) {
    return;
}

int __wrap_amxp_proc_ctrl_new(amxp_proc_ctrl_t** proc, amxp_proc_ctrl_cmd_t cmd_build_fn) {
    int retval = -1;
    when_null(proc, leave);
    when_null(cmd_build_fn, leave);

    *proc = (amxp_proc_ctrl_t*) calloc(1, sizeof(amxp_proc_ctrl_t));
    when_null(*proc, leave);

    when_failed(amxp_subproc_new(&(*proc)->proc), leave);
    when_failed(amxp_timer_new(&(*proc)->timer, test_timer_callback, *proc), leave);
    when_failed(amxc_array_init(&(*proc)->cmd, 10), leave);
    when_failed(amxc_var_init(&(*proc)->child_proc_pids), leave);

    (*proc)->build = cmd_build_fn;
    amxp_slot_connect((*proc)->proc->sigmngr, "stop", NULL, test_stopped, *proc);

    retval = 0;

leave:
    if((retval != 0) &&
       ((proc != NULL) && (*proc != NULL))) {
        amxp_subproc_delete(&(*proc)->proc);
        amxp_timer_delete(&(*proc)->timer);
        amxc_array_clean(&(*proc)->cmd, NULL);
        amxc_var_clean(&(*proc)->child_proc_pids);
        free(*proc);
        *proc = NULL;
    }
    return retval;
}

void __wrap_amxp_proc_ctrl_delete(amxp_proc_ctrl_t** proc) {
    when_null(proc, leave);
    when_null(*proc, leave);

    amxp_slot_disconnect((*proc)->proc->sigmngr, "stop", test_stopped);

    amxp_subproc_delete(&(*proc)->proc);
    amxp_timer_delete(&(*proc)->timer);
    amxc_array_clean(&(*proc)->cmd, test_free_char);
    amxc_var_clean(&(*proc)->child_proc_pids);

    free(*proc);
    *proc = NULL;

leave:
    return;
}

int __wrap_amxp_proc_ctrl_start(amxp_proc_ctrl_t* proc, uint32_t minutes, amxc_var_t* settings) {
    printf("start\n");
    static uint32_t current_pid = 1000;
    int retval = -1;
    when_null(proc, leave);

    amxc_array_clean(&proc->cmd, test_free_char);
    retval = proc->build(&proc->cmd, settings);
    when_failed(retval, leave);

    if(minutes != 0) {
        amxp_timer_start(proc->timer, minutes * 60 * 1000);
    }

    proc->proc->pid = current_pid++;
    proc->proc->is_running = true;
    retval = mock();

leave:
    return retval;
}

int __wrap_amxp_proc_ctrl_stop(amxp_proc_ctrl_t* proc) {
    printf("stop\n");
    int rv = mock();
    amxp_timer_stop(proc->timer);
    amxc_var_t pid;
    amxc_var_init(&pid);
    amxc_var_set(uint32_t, &pid, proc->proc->pid);
    amxp_sigmngr_emit_signal(NULL, "proc:stopped", &pid);
    printf("emit proc stopped\n");
    proc->proc->pid = 0;
    proc->proc->is_running = false;
    return rv;
}

amxb_bus_ctx_t* __wrap_amxb_be_who_has(UNUSED const char* object_path) {
    return (amxb_bus_ctx_t*) 0x1;
}

int __wrap_amxb_get(UNUSED amxb_bus_ctx_t* const bus_ctx,
                    UNUSED const char* object,
                    UNUSED int32_t depth,
                    amxc_var_t* ret,
                    UNUSED int timeout) {
    // [
    //     {
    //         Device.Users.Group.1. = {
    //              Groupname = "root"
    //         }
    //     }
    // ]
    assert_non_null(ret);

    amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
    amxc_var_t* path = amxc_var_add_new(ret);
    amxc_var_set_type(path, AMXC_VAR_ID_HTABLE);
    amxc_var_t* elem = amxc_var_add_new_key(path, "Path");
    amxc_var_set_type(elem, AMXC_VAR_ID_HTABLE);
    amxc_var_t* groupname = amxc_var_add_new_key(elem, "Groupname");
    amxc_var_set(cstring_t, groupname, "testgroup");
    amxc_var_t* username = amxc_var_add_new_key(elem, "Username");
    amxc_var_set(cstring_t, username, "ssh_lan");
    amxc_var_t* uid = amxc_var_add_new_key(elem, "UserID");
    amxc_var_set(uint32_t, uid, 100);
    amxc_var_t* gui = amxc_var_add_new_key(elem, "GroupID");
    amxc_var_set(uint32_t, gui, 100);
    return 0;
}

static void intf_getfirst_cb_ext(const char* intf, ssh_server_instance_t* ssh_server) {
    amxc_var_t data;
    amxc_var_init(&data);
    amxc_var_set(cstring_t, &data, intf);
    intf_getfirst_cb(NULL,
                     &data,
                     ssh_server);
    amxc_var_clean(&data);
}

int test_ssh_server_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;
    amxp_signal_t* signal = NULL;
    amxc_var_t* ssh_server_config = NULL;

    // Not the best approach, but it works
    amxc_var_new(&test_fw_entries);
    amxc_var_set_type(test_fw_entries, AMXC_VAR_ID_HTABLE);

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    assert_int_equal(amxp_signal_new(NULL, &signal, strsignal(SIGCHLD)), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_resolver_ftab_add(&parser, "cleanup_server", AMXO_FUNC(_cleanup_server)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "app_start", AMXO_FUNC(_app_start)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "ssh_server_enable_changed", AMXO_FUNC(_ssh_server_enable_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "ssh_toggle", AMXO_FUNC(_ssh_toggle)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "ssh_server_added", AMXO_FUNC(_ssh_server_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "ssh_server_duration_changed", AMXO_FUNC(_ssh_server_duration_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "ssh_server_interface_changed", AMXO_FUNC(_ssh_server_interface_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "ssh_server_ssh_group_changed", AMXO_FUNC(_ssh_server_ssh_group_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "ssh_server_settings_changed", AMXO_FUNC(_ssh_server_settings_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "ssh_server_authorized_keys_changed", AMXO_FUNC(_ssh_server_authorized_keys_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "close_sessions", AMXO_FUNC(_close_sessions)), 0);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);
    ssh_server_config = amxc_var_add_key(amxc_htable_t, &parser.config, "ssh-server", NULL);
    amxc_var_add_key(bool, ssh_server_config, "auto-restart", false);

    amxc_var_add_key(amxc_htable_t, &parser.config, "ssh-server", NULL);

    _ssh_server_main(0, &dm, &parser);

    handle_events();

    return 0;
}

int test_ssh_server_teardown(UNUSED void** state) {
    _ssh_server_main(1, &dm, &parser);
    // Not the best approach, but it works
    amxc_var_delete(&test_fw_entries);

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    return 0;
}

void test_ssh_server_instances_have_dropbear_ctrl(UNUSED void** state) {
    amxd_object_t* servers = amxd_dm_findf(&dm, "SSH.Server");
    amxd_object_t* server = NULL;

    assert_non_null(servers);

    will_return_always(__wrap_amxp_proc_ctrl_start, 0);
    will_return_always(__wrap_amxp_proc_ctrl_stop, 0);
    amxp_sigmngr_trigger_signal(&dm.sigmngr, "app:start", NULL);

    amxd_object_for_each(instance, it, servers) {
        amxd_object_t* server = amxc_container_of(it, amxd_object_t, it);
        assert_non_null(server->priv);
    }

    server = amxd_dm_findf(&dm, "SSH.Server.1.");
    assert_non_null(server);

    assert_str_attr_equal(server, "State", "Enabled");
    assert_str_attr_equal(server, "Status", "Enabled");

    /* embed an empty ip address transition in the test */

    ssh_server_instance_t* ssh_server = (ssh_server_instance_t*) server->priv;
    intf_getfirst_cb_ext("lan", ssh_server);
    handle_events();
    assert_str_attr_equal(server, "State", "Running");
    assert_str_attr_equal(server, "Status", "Enabled");

    assert_int_equal(check_firewall_ids(ssh_server), 0);

    intf_getfirst_cb_ext("", ssh_server);
    handle_events();
    assert_str_attr_equal(server, "State", "Stopped");
    assert_str_attr_equal(server, "Status", "Disabled");

    intf_getfirst_cb_ext("lan", ssh_server);
    handle_events();
    assert_str_attr_equal(server, "State", "Running");
    assert_str_attr_equal(server, "Status", "Enabled");

    server = amxd_dm_findf(&dm, "SSH.Server.2.");
    assert_non_null(server);
    assert_str_attr_equal(server, "State", "Disabled");
    assert_str_attr_equal(server, "Status", "Disabled");

}

void test_ssh_server_can_be_started(UNUSED void** state) {
    amxd_object_t* server = amxd_dm_findf(&dm, "SSH.Server.2.");
    amxd_trans_t transaction;

    assert_non_null(server);
    will_return_always(__wrap_amxp_proc_ctrl_start, 0);

    assert_int_equal(amxd_object_get_value(uint32_t, server, "PID", NULL), 0);

    amxd_trans_init(&transaction);
    amxd_trans_select_object(&transaction, server);
    amxd_trans_set_value(bool, &transaction, "Enable", true);
    amxd_trans_apply(&transaction, &dm);

    handle_events();

    assert_str_attr_equal(server, "State", "Enabled");
    assert_str_attr_equal(server, "Status", "Enabled");

    ssh_server_instance_t* ssh_server = (ssh_server_instance_t*) server->priv;
    intf_getfirst_cb_ext("lan", ssh_server);

    handle_events();
    assert_str_attr_equal(server, "State", "Running");
    assert_str_attr_equal(server, "Status", "Enabled");

    assert_int_not_equal(amxd_object_get_value(uint32_t, server, "PID", NULL), 0);

    amxd_trans_clean(&transaction);
}

void test_ssh_server_can_be_stopped(UNUSED void** state) {
    amxd_object_t* server = amxd_dm_findf(&dm, "SSH.Server.2.");
    amxd_trans_t transaction;

    assert_non_null(server);
    will_return_always(__wrap_amxp_proc_ctrl_stop, 0);

    assert_int_not_equal(amxd_object_get_value(uint32_t, server, "PID", NULL), 0);

    amxd_trans_init(&transaction);
    amxd_trans_select_object(&transaction, server);
    amxd_trans_set_value(bool, &transaction, "Enable", false);
    amxd_trans_apply(&transaction, &dm);

    handle_events();

    assert_str_attr_equal(server, "State", "Disabled");
    assert_str_attr_equal(server, "Status", "Disabled");

    assert_int_equal(amxd_object_get_value(uint32_t, server, "PID", NULL), 0);

    amxd_trans_clean(&transaction);
}

void test_ssh_server_internal_state_is_error_when_failed_to_start(UNUSED void** state) {
    amxd_object_t* server = amxd_dm_findf(&dm, "SSH.Server.2.");
    amxd_trans_t transaction;

    assert_non_null(server);
    will_return(__wrap_amxp_proc_ctrl_start, -1);

    assert_int_equal(amxd_object_get_value(uint32_t, server, "PID", NULL), 0);

    amxd_trans_init(&transaction);
    amxd_trans_select_object(&transaction, server);
    amxd_trans_set_value(bool, &transaction, "Enable", true);
    amxd_trans_apply(&transaction, &dm);

    handle_events();
    ssh_server_instance_t* ssh_server = (ssh_server_instance_t*) server->priv;
    intf_getfirst_cb_ext("lan", ssh_server);


    handle_events();

    assert_str_attr_equal(server, "State", "Error");
    assert_str_attr_equal(server, "Status", "Error");

    assert_int_equal(amxd_object_get_value(uint32_t, server, "PID", NULL), 0);
    assert_true(amxd_object_get_value(bool, server, "Enable", NULL));

    handle_events();

    amxd_trans_clean(&transaction);
}

void test_no_ssh_server_is_stopped_when_sigchld_with_unkown_pid_is_received(UNUSED void** state) {
    amxd_object_t* server = amxd_dm_findf(&dm, "SSH.Server.1.");
    struct signalfd_siginfo si;
    amxc_var_t sig_var;
    amxp_signal_t* signal = NULL;

    memset(&si, 0, sizeof(struct signalfd_siginfo));
    amxc_var_init(&sig_var);

    assert_non_null(server);

    si.ssi_pid = 1234;
    amxc_var_set(amxp_siginfo_t, &sig_var, &si);

    signal = amxp_sigmngr_find_signal(NULL, strsignal(SIGCHLD));
    assert_non_null(signal);
    amxp_signal_trigger(signal, &sig_var);

    assert_str_attr_equal(server, "State", "Running");
    assert_str_attr_equal(server, "Status", "Enabled");

    assert_int_not_equal(amxd_object_get_value(uint32_t, server, "PID", NULL), 0);
    assert_true(amxd_object_get_value(bool, server, "Enable", NULL));

    handle_events();

    amxc_var_clean(&sig_var);
}

void test_ssh_server_internal_state_is_updated_when_process_is_killed(UNUSED void** state) {
    amxd_object_t* server = amxd_dm_findf(&dm, "SSH.Server.1.");
    amxc_var_t pid;
    amxp_signal_t* signal = NULL;

    amxc_var_init(&pid);
    assert_non_null(server);

    amxc_var_set(uint32_t, &pid, amxd_object_get_value(uint32_t, server, "PID", NULL));

    signal = amxp_sigmngr_find_signal(NULL, "proc:stopped");
    assert_non_null(signal);
    amxp_signal_trigger(signal, &pid);

    assert_str_attr_equal(server, "State", "Stopped");
    assert_str_attr_equal(server, "Status", "Disabled");

    assert_int_equal(amxd_object_get_value(uint32_t, server, "PID", NULL), 0);
    assert_true(amxd_object_get_value(bool, server, "Enable", NULL));

    handle_events();

    amxc_var_clean(&pid);
}

void test_ssh_servers_are_stopped_when_service_is_disabled(UNUSED void** state) {
    amxd_object_t* server = NULL;
    amxd_trans_t transaction;

    will_return_always(__wrap_amxp_proc_ctrl_start, 0);
    will_return_always(__wrap_amxp_proc_ctrl_stop, 0);

    server = amxd_dm_findf(&dm, "SSH.");
    assert_true(amxd_object_get_value(bool, server, "Enable", NULL));

    amxd_trans_init(&transaction);
    amxd_trans_select_pathf(&transaction, "SSH.Server.1.");
    amxd_trans_set_value(bool, &transaction, "Enable", false);
    amxd_trans_select_pathf(&transaction, "SSH.Server.2.");
    amxd_trans_set_value(bool, &transaction, "Enable", false);
    assert_int_equal(amxd_trans_apply(&transaction, &dm), 0);
    amxd_trans_clean(&transaction);

    server = amxd_dm_findf(&dm, "SSH.Server.1.");
    assert_false(amxd_object_get_value(bool, server, "Enable", NULL));
    server = amxd_dm_findf(&dm, "SSH.Server.2.");
    assert_false(amxd_object_get_value(bool, server, "Enable", NULL));

    handle_events();

    amxd_trans_init(&transaction);
    amxd_trans_select_pathf(&transaction, "SSH.Server.1.");
    amxd_trans_set_value(bool, &transaction, "Enable", true);
    amxd_trans_select_pathf(&transaction, "SSH.Server.2.");
    amxd_trans_set_value(bool, &transaction, "Enable", true);
    assert_int_equal(amxd_trans_apply(&transaction, &dm), 0);
    amxd_trans_clean(&transaction);

    server = amxd_dm_findf(&dm, "SSH.Server.1.");
    assert_true(amxd_object_get_value(bool, server, "Enable", NULL));
    server = amxd_dm_findf(&dm, "SSH.Server.2.");
    assert_true(amxd_object_get_value(bool, server, "Enable", NULL));

    handle_events();

    server = amxd_dm_findf(&dm, "SSH.Server.1.");
    assert_non_null(server);
    assert_str_attr_equal(server, "State", "Enabled");
    assert_str_attr_equal(server, "Status", "Enabled");


    ssh_server_instance_t* ssh_server = (ssh_server_instance_t*) server->priv;
    intf_getfirst_cb_ext("lan", ssh_server);

    handle_events();

    assert_str_attr_equal(server, "State", "Running");
    assert_str_attr_equal(server, "Status", "Enabled");

    server = amxd_dm_findf(&dm, "SSH.Server.2.");
    assert_non_null(server);
    assert_str_attr_equal(server, "State", "Enabled");
    assert_str_attr_equal(server, "Status", "Enabled");

    ssh_server = (ssh_server_instance_t*) server->priv;
    intf_getfirst_cb_ext("lan", ssh_server);

    handle_events();

    assert_str_attr_equal(server, "State", "Running");
    assert_str_attr_equal(server, "Status", "Enabled");

    amxd_trans_select_pathf(&transaction, "SSH.");
    amxd_trans_set_value(bool, &transaction, "Enable", false);
    assert_int_equal(amxd_trans_apply(&transaction, &dm), 0);
    amxd_trans_clean(&transaction);
    server = amxd_dm_findf(&dm, "SSH.");
    assert_false(amxd_object_get_value(bool, server, "Enable", NULL));
    handle_events();

    server = amxd_dm_findf(&dm, "SSH.Server.1.");
    assert_non_null(server);
    assert_str_attr_equal(server, "State", "Stopped");
    assert_str_attr_equal(server, "Status", "Disabled");

    assert_true(amxd_object_get_value(bool, server, "Enable", NULL));

    server = amxd_dm_findf(&dm, "SSH.Server.2.");
    assert_non_null(server);
    assert_str_attr_equal(server, "State", "Stopped");
    assert_str_attr_equal(server, "Status", "Disabled");

    assert_true(amxd_object_get_value(bool, server, "Enable", NULL));

    handle_events();
}

void test_ssh_servers_can_be_added_while_service_is_disabled(UNUSED void** state) {
    amxd_object_t* server = NULL;
    amxd_trans_t transaction;

    amxd_trans_init(&transaction);
    amxd_trans_select_pathf(&transaction, "SSH.Server.");
    amxd_trans_add_inst(&transaction, 0, NULL);
    amxd_trans_set_value(bool, &transaction, "Enable", false);
    amxd_trans_set_value(cstring_t, &transaction, "Interface", "lan");
    amxd_trans_select_pathf(&transaction, ".^");
    amxd_trans_add_inst(&transaction, 0, NULL);
    amxd_trans_set_value(bool, &transaction, "Enable", true);
    amxd_trans_set_value(cstring_t, &transaction, "Interface", "lan");
    assert_int_equal(amxd_trans_apply(&transaction, &dm), 0);
    amxd_trans_clean(&transaction);

    handle_events();

    server = amxd_dm_findf(&dm, "SSH.");
    assert_false(amxd_object_get_value(bool, server, "Enable", NULL));

    server = amxd_dm_findf(&dm, "SSH.Server.3.");
    assert_non_null(server);
    assert_str_attr_equal(server, "State", "Disabled");
    assert_str_attr_equal(server, "Status", "Disabled");

    server = amxd_dm_findf(&dm, "SSH.Server.4.");
    assert_non_null(server);
    assert_str_attr_equal(server, "State", "Disabled");
    assert_str_attr_equal(server, "Status", "Disabled");

    amxd_trans_clean(&transaction);
}

void test_ssh_servers_are_started_when_service_is_enabled(UNUSED void** state) {
    amxd_object_t* server = NULL;
    amxd_trans_t transaction;

    will_return_always(__wrap_amxp_proc_ctrl_start, 0);

    server = amxd_dm_findf(&dm, "SSH.");
    assert_false(amxd_object_get_value(bool, server, "Enable", NULL));

    amxd_trans_init(&transaction);
    amxd_trans_select_pathf(&transaction, "SSH.");
    amxd_trans_set_value(bool, &transaction, "Enable", true);
    assert_int_equal(amxd_trans_apply(&transaction, &dm), 0);
    amxd_trans_clean(&transaction);
    server = amxd_dm_findf(&dm, "SSH.");
    assert_true(amxd_object_get_value(bool, server, "Enable", NULL));

    handle_events();

    server = amxd_dm_findf(&dm, "SSH.Server.1.");
    assert_non_null(server);
    assert_str_attr_equal(server, "State", "Enabled");
    assert_str_attr_equal(server, "Status", "Enabled");

    assert_true(amxd_object_get_value(bool, server, "Enable", NULL));

    ssh_server_instance_t* ssh_server = (ssh_server_instance_t*) server->priv;
    intf_getfirst_cb_ext("lan", ssh_server);

    handle_events();

    assert_str_attr_equal(server, "State", "Running");
    assert_str_attr_equal(server, "Status", "Enabled");

    server = amxd_dm_findf(&dm, "SSH.Server.2.");
    assert_non_null(server);
    assert_str_attr_equal(server, "State", "Enabled");
    assert_str_attr_equal(server, "Status", "Enabled");

    assert_true(amxd_object_get_value(bool, server, "Enable", NULL));

    ssh_server = (ssh_server_instance_t*) server->priv;
    intf_getfirst_cb_ext("lan", ssh_server);

    handle_events();

    assert_str_attr_equal(server, "State", "Running");
    assert_str_attr_equal(server, "Status", "Enabled");

    server = amxd_dm_findf(&dm, "SSH.Server.3.");
    assert_non_null(server);
    assert_str_attr_equal(server, "State", "Disabled");
    assert_str_attr_equal(server, "Status", "Disabled");

    assert_false(amxd_object_get_value(bool, server, "Enable", NULL));

    server = amxd_dm_findf(&dm, "SSH.Server.4.");
    assert_non_null(server);
    assert_str_attr_equal(server, "State", "Enabled");
    assert_str_attr_equal(server, "Status", "Enabled");

    assert_true(amxd_object_get_value(bool, server, "Enable", NULL));

    handle_events();
}

void test_ssh_server_active_duration_can_be_set_after_start(UNUSED void** state) {
    amxd_object_t* server = NULL;
    amxd_trans_t transaction;
    amxp_proc_ctrl_t* dropbear_ctrl = NULL;

    will_return_always(__wrap_amxp_proc_ctrl_stop, 0);

    server = amxd_dm_findf(&dm, "SSH.Server.1.");
    assert_non_null(server);
    assert_str_attr_equal(server, "State", "Running");
    assert_str_attr_equal(server, "Status", "Enabled");
    assert_true(amxd_object_get_value(bool, server, "Enable", NULL));
    assert_int_equal(amxd_object_get_value(uint32_t, server, "AutoDisableDuration", NULL), 0);

    ssh_server_instance_t* ssh_server = (ssh_server_instance_t*) server->priv;
    dropbear_ctrl = (amxp_proc_ctrl_t*) ssh_server->proc_ctrl;
    assert_int_not_equal(amxp_timer_get_state(dropbear_ctrl->timer), amxp_timer_running);

    amxd_trans_init(&transaction);
    amxd_trans_select_pathf(&transaction, "SSH.Server.1.");
    amxd_trans_set_value(uint32_t, &transaction, "AutoDisableDuration", 1);
    assert_int_equal(amxd_trans_apply(&transaction, &dm), 0);
    amxd_trans_clean(&transaction);

    handle_events();
    amxp_timers_calculate();
    amxp_timers_check();

    assert_int_equal(amxp_timer_get_state(dropbear_ctrl->timer), amxp_timer_running);

    read_sigalrm();
    amxp_timers_calculate();
    amxp_timers_check();
    handle_events();

    assert_str_attr_equal(server, "State", "Disabled");
    assert_str_attr_equal(server, "Status", "Disabled");

    assert_false(amxd_object_get_value(bool, server, "Enable", NULL));
    assert_int_not_equal(amxp_timer_get_state(dropbear_ctrl->timer), amxp_timer_running);
}

void test_ssh_server_is_not_restarted_if_auto_restart_is_off(UNUSED void** state) {
    amxd_object_t* server = NULL;
    amxd_trans_t transaction;
    amxp_proc_ctrl_t* dropbear_ctrl = NULL;
    uint32_t pid = 0;

    will_return_always(__wrap_amxp_proc_ctrl_start, 0);

    amxd_trans_init(&transaction);
    amxd_trans_select_pathf(&transaction, "SSH.Server.1.");
    amxd_trans_set_value(uint32_t, &transaction, "AutoDisableDuration", 0);
    amxd_trans_set_value(bool, &transaction, "Enable", true);
    assert_int_equal(amxd_trans_apply(&transaction, &dm), 0);
    amxd_trans_clean(&transaction);

    handle_events();

    server = amxd_dm_findf(&dm, "SSH.Server.1.");
    assert_non_null(server);
    assert_str_attr_equal(server, "State", "Enabled");
    assert_str_attr_equal(server, "Status", "Enabled");

    ssh_server_instance_t* ssh_server = (ssh_server_instance_t*) server->priv;
    intf_getfirst_cb_ext("lan", ssh_server);

    handle_events();

    assert_str_attr_equal(server, "State", "Running");
    assert_str_attr_equal(server, "Status", "Enabled");

    dropbear_ctrl = (amxp_proc_ctrl_t*) ssh_server->proc_ctrl;
    assert_true(amxp_subproc_is_running(dropbear_ctrl->proc));
    pid = dropbear_ctrl->proc->pid;

    amxd_trans_init(&transaction);
    amxd_trans_select_pathf(&transaction, "SSH.Server.1.");
    amxd_trans_set_value(uint32_t, &transaction, "Port", 2020);
    assert_int_equal(amxd_trans_apply(&transaction, &dm), 0);
    amxd_trans_clean(&transaction);

    handle_events();

    assert_str_attr_equal(server, "State", "Running");
    assert_str_attr_equal(server, "Status", "Enabled");

    assert_true(amxp_subproc_is_running(dropbear_ctrl->proc));
    assert_int_equal(pid, dropbear_ctrl->proc->pid);
}

void test_ssh_server_is_restarted_if_settings_changed(UNUSED void** state) {
    amxd_object_t* server = NULL;
    amxd_trans_t transaction;
    amxp_proc_ctrl_t* dropbear_ctrl = NULL;
    uint32_t pid = 0;

    amxc_var_t* auto_restart = GETP_ARG(&parser.config, "ssh-server.auto-restart");
    amxc_var_set(bool, auto_restart, true);

    will_return_always(__wrap_amxp_proc_ctrl_start, 0);
    will_return_always(__wrap_amxp_proc_ctrl_stop, 0);

    amxd_trans_init(&transaction);
    amxd_trans_select_pathf(&transaction, "SSH.Server.1.");
    amxd_trans_set_value(uint32_t, &transaction, "AutoDisableDuration", 0);
    amxd_trans_set_value(bool, &transaction, "Enable", true);
    assert_int_equal(amxd_trans_apply(&transaction, &dm), 0);
    amxd_trans_clean(&transaction);

    handle_events();

    server = amxd_dm_findf(&dm, "SSH.Server.1.");
    assert_non_null(server);
    assert_str_attr_equal(server, "State", "Running");
    assert_str_attr_equal(server, "Status", "Enabled");


    ssh_server_instance_t* ssh_server = (ssh_server_instance_t*) server->priv;
    dropbear_ctrl = (amxp_proc_ctrl_t*) ssh_server->proc_ctrl;
    assert_true(amxp_subproc_is_running(dropbear_ctrl->proc));
    pid = dropbear_ctrl->proc->pid;
    ipversion = 4;
    amxd_trans_init(&transaction);
    amxd_trans_select_pathf(&transaction, "SSH.Server.1.");
    amxd_trans_set_value(uint32_t, &transaction, "Port", 22);
    amxd_trans_set_value(bool, &transaction, "AllowRootPasswordLogin", true);
    amxd_trans_set_value(bool, &transaction, "AllowRootLogin", false);
    amxd_trans_set_value(bool, &transaction, "AllowPasswordLogin", false);
    amxd_trans_set_value(cstring_t, &transaction, "IPv4AllowedSourcePrefix", "0.0.0.0");
    amxd_trans_set_value(uint8_t, &transaction, "IPVersion", 4);
    assert_int_equal(amxd_trans_apply(&transaction, &dm), 0);
    amxd_trans_clean(&transaction);

    handle_events();

    assert_str_attr_equal(server, "State", "Enabled");
    assert_str_attr_equal(server, "Status", "Enabled");

    intf_getfirst_cb_ext("lan", ssh_server);
    handle_events();

    assert_str_attr_equal(server, "State", "Running");
    assert_str_attr_equal(server, "Status", "Enabled");

    check_firewall_ids(ssh_server);

    dropbear_ctrl = (amxp_proc_ctrl_t*) ssh_server->proc_ctrl;
    assert_true(amxp_subproc_is_running(dropbear_ctrl->proc));
    assert_int_not_equal(pid, dropbear_ctrl->proc->pid);

    ipversion = 0;
    amxd_trans_init(&transaction);
    amxd_trans_select_pathf(&transaction, "SSH.Server.1.");
    amxd_trans_set_value(uint8_t, &transaction, "IPVersion", 0);
    assert_int_equal(amxd_trans_apply(&transaction, &dm), 0);
    amxd_trans_clean(&transaction);

    handle_events();

    assert_str_attr_equal(server, "State", "Enabled");
    assert_str_attr_equal(server, "Status", "Enabled");

    intf_getfirst_cb_ext("lan", ssh_server);
    handle_events();

    assert_str_attr_equal(server, "State", "Running");
    assert_str_attr_equal(server, "Status", "Enabled");

    check_firewall_ids(ssh_server);
}

void test_ssh_server_can_stop_sessions(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxd_object_t* server = amxd_dm_findf(&dm, "SSH.Server.1.");

    assert_int_equal(amxd_object_invoke_function(server, "close_sessions", &args, &ret), 0);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_ssh_server_destroy_action_can_not_be_used_for_other_action(UNUSED void** state) {
    amxc_var_t retval;
    amxd_object_t* server = amxd_dm_findf(&dm, "SSH.Server.1.");
    amxc_var_init(&retval);

    assert_int_equal(_cleanup_server(server, NULL, action_object_validate, NULL, &retval, NULL),
                     amxd_status_function_not_implemented);
    assert_int_equal(_cleanup_server(server, NULL, action_param_destroy, NULL, &retval, NULL),
                     amxd_status_function_not_implemented);

    amxc_var_clean(&retval);
}

void test_ssh_server_allow_all_ipvx(UNUSED void** state) {
    amxd_object_t* server = NULL;
    amxd_trans_t transaction;
    amxc_var_t* fw_entry = NULL;
    ssh_server_instance_t* ssh_server = NULL;

    amxd_trans_init(&transaction);
    amxd_trans_select_pathf(&transaction, "SSH.Server.");
    amxd_trans_add_inst(&transaction, 0, "beast");
    amxd_trans_set_value(bool, &transaction, "Enable", true);
    amxd_trans_set_value(bool, &transaction, "AllowAllIPv4", true);
    amxd_trans_set_value(bool, &transaction, "AllowAllIPv6", true);
    amxd_trans_set_value(uint8_t, &transaction, "IPVersion", 0);

    amxd_trans_set_value(cstring_t, &transaction, "Interface", "beast");
    amxd_trans_set_value(cstring_t, &transaction, "IPv4AllowedSourcePrefix", "192.168.0.0/24");
    amxd_trans_set_value(cstring_t, &transaction, "IPv6AllowedSourcePrefix", "2001:db8:1234:72a2::/64");

    assert_int_equal(amxd_trans_apply(&transaction, &dm), 0);
    amxd_trans_clean(&transaction);

    handle_events();

    server = amxd_dm_findf(&dm, "SSH.Server.beast.");
    assert_non_null(server);
    ssh_server = (ssh_server_instance_t*) server->priv;
    assert_non_null(ssh_server);

    // Checking IPvX, first all allowed
    assert_int_equal(amxd_object_get_value(uint8_t, server, "IPVersion", NULL), 0);
    assert_true(amxd_object_get_value(bool, server, "AllowAllIPv4", NULL));
    assert_true(amxd_object_get_value(bool, server, "AllowAllIPv6", NULL));

    // All IPv4, hence empty IPv4AllowedSourcePrefix in FW setting
    assert_str_attr_equal(server, "IPv4AllowedSourcePrefix", "192.168.0.0/24");
    assert_str_attr_equal(server, "IPv6AllowedSourcePrefix", "2001:db8:1234:72a2::/64");

    will_return_always(__wrap_amxp_proc_ctrl_start, 0);
    intf_getfirst_cb_ext("beast", ssh_server);
    handle_events();

    // IPv4
    assert_non_null(ssh_server->ipv4_fw_serviceid);
    fw_entry = amxc_var_get_key(test_fw_entries, ssh_server->ipv4_fw_serviceid, 0);
    assert_non_null(fw_entry);

    // No prefix should be used, because AllowAllIPv4
    assert_string_equal(GETP_CHAR(fw_entry, "source_prefix"), "");

    // IPv6
    assert_non_null(ssh_server->ipv6_fw_serviceid);
    fw_entry = amxc_var_get_key(test_fw_entries, ssh_server->ipv4_fw_serviceid, 0);
    assert_non_null(fw_entry);

    // No prefix should be used, because AllowAllIPv4
    assert_string_equal(GETP_CHAR(fw_entry, "source_prefix"), "");

    // Switch AllowAllIPv4 to false:
    amxd_trans_init(&transaction);
    amxd_trans_select_pathf(&transaction, "SSH.Server.beast");
    amxd_trans_set_value(bool, &transaction, "AllowAllIPv4", false);
    amxd_trans_set_value(bool, &transaction, "AllowAllIPv6", false);
    assert_int_equal(amxd_trans_apply(&transaction, &dm), 0);
    amxd_trans_clean(&transaction);

    will_return_always(__wrap_amxp_proc_ctrl_stop, 0);
    handle_events();

    // IPv4
    assert_non_null(ssh_server->ipv4_fw_serviceid);
    fw_entry = amxc_var_get_key(test_fw_entries, ssh_server->ipv4_fw_serviceid, 0);
    assert_non_null(fw_entry);

    // No prefix should be used, because AllowAllIPv4
    assert_string_equal(GETP_CHAR(fw_entry, "source_prefix"), "192.168.0.0/24");

    // IPv6
    assert_non_null(ssh_server->ipv6_fw_serviceid);
    fw_entry = amxc_var_get_key(test_fw_entries, ssh_server->ipv6_fw_serviceid, 0);
    assert_non_null(fw_entry);

    // No prefix should be used, because AllowAllIPv4
    assert_string_equal(GETP_CHAR(fw_entry, "source_prefix"), "2001:db8:1234:72a2::/64");
}

void test_ssh_group_settings(UNUSED void** state) {
    // Check parameter for group
    // Check vendor prefix
    amxd_object_t* server = NULL;
    ssh_server_instance_t* ssh_server = NULL;

    server = amxd_dm_findf(&dm, "SSH.Server.1.");
    assert_non_null(server);

    // Note the prefix here!
    assert_str_attr_equal(server, VENDOR_PREFIX "GroupRestriction", "Device.Users.Group.root-group");

    ssh_server = (ssh_server_instance_t*) server->priv;

    assert_non_null(ssh_server->ssh_group);
    assert_string_equal(ssh_server->ssh_group, "testgroup");
}

void test_ssh_add_authkey(UNUSED void** state) {
    amxd_object_t* authKey = NULL;
    amxd_trans_t transaction;
    bool found1 = false;
    bool found2 = false;
    char* line = NULL;
    size_t len = 0;
    ssize_t read = 0;
    amxc_string_t str;

    mkdir("/home/ssh_lan", 0700);
    FILE* file = fopen("/home/ssh_lan/.ssh/authorized_keys", "r");
    assert_null(file);
    if(file) {
        fclose(file);
    }

    will_return_always(__wrap_amxp_proc_ctrl_stop, 0);

    amxd_trans_init(&transaction);
    amxd_trans_select_pathf(&transaction, "SSH.AuthorizedKey.");
    amxd_trans_add_inst(&transaction, 0, "test");
    amxd_trans_set_value(cstring_t, &transaction, "User", "Device.Users.User.ssh_lan-user.");
    amxd_trans_set_value(cstring_t, &transaction, "Key", "key");
    assert_int_equal(amxd_trans_apply(&transaction, &dm), 0);
    amxd_trans_clean(&transaction);
    handle_events();

    authKey = amxd_dm_findf(&dm, "SSH.AuthorizedKey.test.");
    assert_non_null(authKey);

    assert_str_attr_equal(authKey, "User", "Device.Users.User.ssh_lan-user.");
    assert_str_attr_equal(authKey, "Key", "key");

    file = fopen("/home/ssh_lan/.ssh/authorized_keys", "r");
    assert_non_null(file);

    amxc_string_init(&str, 0);

    while((read = getline(&line, &len, file)) != -1) {
        if(read > 0) {
            amxc_string_set(&str, line);
            if(amxc_string_search(&str, "key", 0) != -1) {
                found1 = true;
                break;
            }
            amxc_string_reset(&str);
        }
    }

    amxc_string_clean(&str);
    fclose(file);
    file = NULL;
    assert_true(found1);

    amxd_trans_init(&transaction);
    amxd_trans_select_pathf(&transaction, "SSH.AuthorizedKey.");
    amxd_trans_add_inst(&transaction, 0, "anotherTest");
    amxd_trans_set_value(cstring_t, &transaction, "User", "Device.Users.User.ssh_lan-user.");
    amxd_trans_set_value(cstring_t, &transaction, "Key", "anotherKey");
    assert_int_equal(amxd_trans_apply(&transaction, &dm), 0);
    amxd_trans_clean(&transaction);
    handle_events();

    authKey = amxd_dm_findf(&dm, "SSH.AuthorizedKey.anotherTest.");
    assert_non_null(authKey);

    assert_str_attr_equal(authKey, "User", "Device.Users.User.ssh_lan-user.");
    assert_str_attr_equal(authKey, "Key", "anotherKey");

    found1 = false;
    found2 = false;

    file = fopen("/home/ssh_lan/.ssh/authorized_keys", "r");
    assert_non_null(file);
    amxc_string_init(&str, 0);

    while((read = getline(&line, &len, file)) != -1) {
        if(read > 0) {
            amxc_string_set(&str, line);
            if(amxc_string_search(&str, "key", 0) != -1) {
                found1 = true;
            }
            if(amxc_string_search(&str, "key", 0) != -1) {
                found2 = true;
            }
            if(found1 && found2) {
                break;
            }
            amxc_string_reset(&str);
        }
    }
    amxc_string_clean(&str);
    free(line);
    fclose(file);
    assert_true(found1);
    assert_true(found2);
}

void test_ssh_del_authkey(UNUSED void** state) {
    amxd_object_t* authKey = NULL;
    amxd_trans_t transaction;
    bool found = false;
    char* line = NULL;
    size_t len = 0;
    ssize_t read = 0;
    amxc_string_t str;

    amxd_trans_init(&transaction);
    amxd_trans_select_pathf(&transaction, "SSH.AuthorizedKey.");
    amxd_trans_del_inst(&transaction, 0, "test");
    assert_int_equal(amxd_trans_apply(&transaction, &dm), 0);
    amxd_trans_clean(&transaction);
    handle_events();

    authKey = amxd_dm_findf(&dm, "SSH.AuthorizedKey.test.");
    assert_null(authKey);

    FILE* file = fopen("/home/ssh_lan/.ssh/authorized_keys", "r");
    assert_non_null(file);
    amxc_string_init(&str, 0);
    while((read = getline(&line, &len, file)) != -1) {
        if(read > 0) {
            amxc_string_set(&str, line);
            if(amxc_string_search(&str, "key", 0) != -1) {
                found = true;
                break;
            }
            amxc_string_reset(&str);
        }
    }

    fclose(file);
    file = NULL;
    amxc_string_clean(&str);
    free(line);
    assert_true(!found);

    amxd_trans_init(&transaction);
    amxd_trans_select_pathf(&transaction, "SSH.AuthorizedKey.");
    amxd_trans_del_inst(&transaction, 0, "anotherTest");
    assert_int_equal(amxd_trans_apply(&transaction, &dm), 0);
    amxd_trans_clean(&transaction);
    handle_events();

    authKey = amxd_dm_findf(&dm, "SSH.AuthorizedKey.anotherTest.");
    assert_null(authKey);

    file = fopen("/home/ssh_lan/.ssh/authorized_keys", "r");
    assert_null(file);
}

void test_dropbear_cmd_args(UNUSED void** state) {
    amxd_object_t* server = NULL;
    amxp_proc_ctrl_t* dropbear_ctrl = NULL;
    ssh_server_instance_t* ssh_server = NULL;
    amxc_array_it_t* cmd_it = NULL;

    /**
     * This test checks for the arguments passed to the dropbear executable.
     * These values depend on the rest of the tests. If a default value changes,
     * or another setting is modified (eg. Port or AllowPasswordLogin), then this
     * test will fail.
     *
     * It can be queried automatically from the datamodel, but then unwanted changes
     * to the datamodel can be unnoticeable.
     */
    server = amxd_dm_findf(&dm, "SSH.Server.1.");
    assert_non_null(server);

    ssh_server = (ssh_server_instance_t*) server->priv;
    assert_non_null(ssh_server);

    dropbear_ctrl = (amxp_proc_ctrl_t*) ssh_server->proc_ctrl;
    assert_non_null(dropbear_ctrl);

    assert_non_null(&dropbear_ctrl->cmd);

    // For the command we expect for the first server:
    // dropbear -F -E -s -w -T3 -p22 -llan -K300 -I180
    // -F Don't fork into background
    // -E Log to stderr rather than syslog
    // -s Disable password logins (AllowPasswordLogin=false)
    // -w Disallow root logins (AllowPasswordLogin=false)
    // NO -g Disable password logins for root (AllowRootPasswordLogin=true)
    // -T Maximum authentication tries (default 3)
    // -p Listen on specified tcp port
    // -l Listen on specified interface
    // -K KeepAlive
    // -I IdleTimeout
    // -G User group with ssh access
    cmd_it = amxc_array_get_first(&dropbear_ctrl->cmd);
    assert_string_equal((const char*) amxc_array_it_get_data(cmd_it), "dropbear");
    cmd_it = amxc_array_it_get_next(cmd_it);
    assert_string_equal((const char*) amxc_array_it_get_data(cmd_it), "-F");
    cmd_it = amxc_array_it_get_next(cmd_it);
    assert_string_equal((const char*) amxc_array_it_get_data(cmd_it), "-E");
    cmd_it = amxc_array_it_get_next(cmd_it);
    assert_string_equal((const char*) amxc_array_it_get_data(cmd_it), "-R");
    cmd_it = amxc_array_it_get_next(cmd_it);
    assert_string_equal((const char*) amxc_array_it_get_data(cmd_it), "-s");
    cmd_it = amxc_array_it_get_next(cmd_it);
    assert_string_equal((const char*) amxc_array_it_get_data(cmd_it), "-w");
    cmd_it = amxc_array_it_get_next(cmd_it);
    assert_string_equal((const char*) amxc_array_it_get_data(cmd_it), "-T3");
    cmd_it = amxc_array_it_get_next(cmd_it);
    assert_string_equal((const char*) amxc_array_it_get_data(cmd_it), "-p22");
    cmd_it = amxc_array_it_get_next(cmd_it);
    assert_string_equal((const char*) amxc_array_it_get_data(cmd_it), "-llan");
    cmd_it = amxc_array_it_get_next(cmd_it);
    assert_string_equal((const char*) amxc_array_it_get_data(cmd_it), "-K300");
    cmd_it = amxc_array_it_get_next(cmd_it);
    assert_string_equal((const char*) amxc_array_it_get_data(cmd_it), "-I180");
    cmd_it = amxc_array_it_get_next(cmd_it);
    assert_string_equal((const char*) amxc_array_it_get_data(cmd_it), "-Gtestgroup");
    cmd_it = amxc_array_it_get_next(cmd_it);
    assert_string_equal((const char*) amxc_array_it_get_data(cmd_it), "-r/etc/config/ssh_server/dropbear_rsa_host_key");
    cmd_it = amxc_array_it_get_next(cmd_it);
    assert_string_equal((const char*) amxc_array_it_get_data(cmd_it), "-r/etc/config/ssh_server/dropbear_ecdsa_host_key");
    cmd_it = amxc_array_it_get_next(cmd_it);
    assert_string_equal((const char*) amxc_array_it_get_data(cmd_it), "-r/etc/config/ssh_server/dropbear_ed25519_host_key");
    cmd_it = amxc_array_it_get_next(cmd_it);
    assert_null(cmd_it);

    // SECOND SERVER
    server = amxd_dm_findf(&dm, "SSH.Server.2.");
    assert_non_null(server);

    ssh_server = (ssh_server_instance_t*) server->priv;
    assert_non_null(ssh_server);

    dropbear_ctrl = (amxp_proc_ctrl_t*) ssh_server->proc_ctrl;
    assert_non_null(dropbear_ctrl);

    assert_non_null(&dropbear_ctrl->cmd);

    cmd_it = amxc_array_get_first(&dropbear_ctrl->cmd);
    assert_string_equal((const char*) amxc_array_it_get_data(cmd_it), "dropbear");
    cmd_it = amxc_array_it_get_next(cmd_it);
    assert_string_equal((const char*) amxc_array_it_get_data(cmd_it), "-F");
    cmd_it = amxc_array_it_get_next(cmd_it);
    assert_string_equal((const char*) amxc_array_it_get_data(cmd_it), "-E");
    cmd_it = amxc_array_it_get_next(cmd_it);
    assert_string_equal((const char*) amxc_array_it_get_data(cmd_it), "-R");
    cmd_it = amxc_array_it_get_next(cmd_it);
    assert_string_equal((const char*) amxc_array_it_get_data(cmd_it), "-s");
    cmd_it = amxc_array_it_get_next(cmd_it);
    assert_string_equal((const char*) amxc_array_it_get_data(cmd_it), "-w");
    cmd_it = amxc_array_it_get_next(cmd_it);
    assert_string_equal((const char*) amxc_array_it_get_data(cmd_it), "-g");
    cmd_it = amxc_array_it_get_next(cmd_it);
    assert_string_equal((const char*) amxc_array_it_get_data(cmd_it), "-T3");
    cmd_it = amxc_array_it_get_next(cmd_it);
    assert_string_equal((const char*) amxc_array_it_get_data(cmd_it), "-p2223");
    cmd_it = amxc_array_it_get_next(cmd_it);
    assert_string_equal((const char*) amxc_array_it_get_data(cmd_it), "-llan");
    cmd_it = amxc_array_it_get_next(cmd_it);
    assert_string_equal((const char*) amxc_array_it_get_data(cmd_it), "-K300");
    cmd_it = amxc_array_it_get_next(cmd_it);
    assert_string_equal((const char*) amxc_array_it_get_data(cmd_it), "-I180");
    cmd_it = amxc_array_it_get_next(cmd_it);
    assert_string_equal((const char*) amxc_array_it_get_data(cmd_it), "-r/etc/config/ssh_server/dropbear_rsa_host_key");
    cmd_it = amxc_array_it_get_next(cmd_it);
    assert_string_equal((const char*) amxc_array_it_get_data(cmd_it), "-r/etc/config/ssh_server/dropbear_ecdsa_host_key");
    cmd_it = amxc_array_it_get_next(cmd_it);
    assert_string_equal((const char*) amxc_array_it_get_data(cmd_it), "-r/etc/config/ssh_server/dropbear_ed25519_host_key");
    cmd_it = amxc_array_it_get_next(cmd_it);
    assert_null(cmd_it); // No group configured
}
