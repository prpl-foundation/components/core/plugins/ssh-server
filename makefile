include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C src all

clean:
	$(MAKE) -C src clean

install: all
	$(INSTALL) -D -p -m 0644 odl/ssh_server.odl $(DEST)/etc/amx/ssh_server/ssh_server.odl
	$(INSTALL) -D -p -m 0644 odl/ssh_server_definition.odl $(DEST)/etc/amx/ssh_server/ssh_server_definition.odl
	$(INSTALL) -D -p -m 0644 odl/ssh_server_extra.odl $(DEST)/etc/amx/ssh_server/ssh_server_extra.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_mapping.odl $(DEST)/etc/amx/tr181-device/extensions/01_device-ssh-server_mapping.odl
ifeq ($(CONFIG_GATEWAY),y)
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/ssh_server/defaults.gw.d
	$(foreach odl,$(wildcard odl/defaults.gw.d/*.odl), $(INSTALL) -D -p -m 0644 $(odl) $(DEST)/etc/amx/ssh_server/defaults.gw.d/;)
endif
ifeq ($(CONFIG_ACCESSPOINT),y)
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/ssh_server/defaults.ap.d
	$(foreach odl,$(wildcard odl/defaults.ap.d/*.odl), $(INSTALL) -D -p -m 0644 $(odl) $(DEST)/etc/amx/ssh_server/defaults.ap.d/;)
endif
ifeq ($(CONFIG_GATEWAY),y)
	$(INSTALL) -d -m 0755 $(DEST)/etc/amx/ssh_server
	ln -sfr $(DEST)/etc/amx/ssh_server/defaults.gw.d $(DEST)/etc/amx/ssh_server/defaults.d
endif
ifeq ($(and $(CONFIG_ACCESSPOINT),$(if $(CONFIG_GATEWAY),,y)),y)
	$(INSTALL) -d -m 0755 $(DEST)/etc/amx/ssh_server
	ln -sfr $(DEST)/etc/amx/ssh_server/defaults.ap.d $(DEST)/etc/amx/ssh_server/defaults.d
endif
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/object/ssh-server.so $(DEST)/usr/lib/amx/ssh_server/ssh_server.so
	$(INSTALL) -d -m 0755 $(DEST)$(BINDIR)
	ln -sfr $(DEST)$(BINDIR)/amxrt $(DEST)$(BINDIR)/ssh_server
	$(INSTALL) -D -p -m 0755 scripts/ssh-server.sh $(DEST)$(INITDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/reset_stop_ssh.sh $(DEST)/usr/bin/reset_hard_ssh
	$(INSTALL) -D -p -m 0755 scripts/reset_stop_ssh.sh $(DEST)/usr/bin/reset_soft_ssh
	$(INSTALL) -D -p -m 0660 acl/cwmpd/$(COMPONENT).json $(DEST)$(ACLDIR)/cwmpd/$(COMPONENT).json

package: all
	$(INSTALL) -D -p -m 0644 odl/ssh_server.odl $(PKGDIR)/etc/amx/ssh_server/ssh_server.odl
	$(INSTALL) -D -p -m 0644 odl/ssh_server_definition.odl $(PKGDIR)/etc/amx/ssh_server/ssh_server_definition.odl
	$(INSTALL) -D -p -m 0644 odl/ssh_server_extra.odl $(PKGDIR)/etc/amx/ssh_server/ssh_server_extra.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_mapping.odl $(PKGDIR)/etc/amx/tr181-device/extensions/01_device-ssh-server_mapping.odl
ifeq ($(CONFIG_GATEWAY),y)
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/ssh_server/defaults.gw.d
	$(INSTALL) -D -p -m 0644 odl/defaults.gw.d/*.odl $(PKGDIR)/etc/amx/ssh_server/defaults.gw.d/
endif
ifeq ($(CONFIG_ACCESSPOINT),y)
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/ssh_server/defaults.ap.d
	$(INSTALL) -D -p -m 0644 odl/defaults.ap.d/*.odl $(PKGDIR)/etc/amx/ssh_server/defaults.ap.d/
endif
ifeq ($(CONFIG_GATEWAY),y)
	$(INSTALL) -d -m 0755 $(PKGDIR)/etc/amx/ssh_server
	rm -f $(PKGDIR)/etc/amx/ssh_server/defaults.d
	ln -sfr $(PKGDIR)/etc/amx/ssh_server/defaults.gw.d $(PKGDIR)/etc/amx/ssh_server/defaults.d
endif
ifeq ($(and $(CONFIG_ACCESSPOINT),$(if $(CONFIG_GATEWAY),,y)),y)
	$(INSTALL) -d -m 0755 $(PKGDIR)/etc/amx/ssh_server
	rm -f $(PKGDIR)/etc/amx/ssh_server/defaults.d
	ln -sfr $(PKGDIR)/etc/amx/ssh_server/defaults.ap.d $(PKGDIR)/etc/amx/ssh_server/defaults.d
endif
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/object/ssh-server.so $(PKGDIR)/usr/lib/amx/ssh_server/ssh_server.so
	$(INSTALL) -d -m 0755 $(PKGDIR)$(BINDIR)
	rm -f $(PKGDIR)$(BINDIR)/ssh_server
	ln -sfr $(PKGDIR)$(BINDIR)/amxrt $(PKGDIR)$(BINDIR)/ssh_server
	$(INSTALL) -D -p -m 0755 scripts/ssh-server.sh $(PKGDIR)$(INITDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/reset_stop_ssh.sh $(PKGDIR)/usr/bin/reset_hard_ssh
	$(INSTALL) -D -p -m 0755 scripts/reset_stop_ssh.sh $(PKGDIR)/usr/bin/reset_soft_ssh
	$(INSTALL) -D -p -m 0660 acl/cwmpd/$(COMPONENT).json $(PKGDIR)$(ACLDIR)/cwmpd/$(COMPONENT).json
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

doc:
	$(eval ODLFILES += odl/ssh_server_definition.odl)

	mkdir -p output/xml
	mkdir -p output/html
	mkdir -p output/confluence
	amxo-cg -Gxml,output/xml/$(COMPONENT).xml $(or $(ODLFILES), "")
	amxo-xml-to -x html -o output-dir=output/html -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml
	amxo-xml-to -x confluence -o output-dir=output/confluence -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml

test:
	$(MAKE) -C test run
	$(MAKE) -C test coverage

.PHONY: all clean changelog install package doc test